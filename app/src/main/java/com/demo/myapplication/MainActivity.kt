package com.demo.myapplication

import android.os.Bundle
import android.widget.ListView
import android.app.Activity
import android.widget.Toast
import com.usizy.sizequeryandroid.model.*
import com.usizy.sizebutton.listeners.UsizyButtonListenerAdapter

class MainActivity : Activity(), UsizyButtonListenerAdapter {
    private val productIds = arrayOf(
        "BOY_SHOES",
        "FEMALE_SHOES",
        "UNISEX_SHOES",
        "MALE_SHOES",
        "HUMAN_SHOES",
        "BABY_UPPER",
        "BOY_FULL",
        "BOY_LOWER",
        "BOY_UPPER",
        "CHILD_FULL",
        "CHILD_UPPER",
        "FEMALE_FULL",
        "FEMALE_LOWER",
        "FEMALE_UPPER",
        "GIRL_FULL",
        "GIRL_LOWER",
        "GIRL_UPPER",
        "HUMAN",
        "MALE_FULL",
        "MALE_LOWER",
        "MALE_UPPER",
        "PANT_WOMAN",
        "UNISEX_UPPER",
        "UNISEX",
        "UNISEX_FULL",
        "UNISEX_LOWER",
        "HUMAN_UPPER"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val myListAdapter = MyListAdapter(this, productIds)
        findViewById<ListView>(R.id.listView).adapter = myListAdapter
    }

    override fun onRecommendedSize(productId: String?, data: SaveRecommendResponse?) {
        Toast.makeText(this, "$productId: ${data?.usizySize}", Toast.LENGTH_SHORT).show()
    }
}





