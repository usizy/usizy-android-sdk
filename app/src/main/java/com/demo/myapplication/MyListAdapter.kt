package com.demo.myapplication

import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.usizy.sizebutton.UsizyButton
import com.usizy.sizebutton.model.UsizyButtonConfiguration

class MyListAdapter(private val context: MainActivity, private val productIds: Array<String>)
    : ArrayAdapter<String>(context, R.layout.custom_list, productIds) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.custom_list, null, true)

        val productId = productIds[position]

        val titleText = rowView.findViewById(R.id.title) as TextView
        titleText.text = productId

        val btnUsizy = rowView.findViewById<UsizyButton>(R.id.btnUsizy)
        val config = UsizyButtonConfiguration()
        config.productId = productId
        config.user = "AnonymousUser"
        btnUsizy.initialize(config)

        btnUsizy.setOnSizeListener(context)

        return rowView
    }
}