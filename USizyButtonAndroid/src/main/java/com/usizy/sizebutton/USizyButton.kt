package com.usizy.sizebutton

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import com.usizy.sizebutton.cache.SizeCache
import com.usizy.sizebutton.constants.ButtonAttributes
import com.usizy.sizebutton.constants.LOGO_RES_ID_KEY
import com.usizy.sizebutton.constants.PRODUCT_DATA_KEY
import com.usizy.sizebutton.constants.USER_RES_ID_KEY
import com.usizy.sizebutton.events.RecommendSizeEvent
import com.usizy.sizebutton.listeners.UsizyButtonListenerAdapter
import com.usizy.sizebutton.model.UsizyButtonConfiguration
import com.usizy.sizebutton.ui.FormActivity
import com.usizy.sizequeryandroid.ShowProduct
import com.usizy.sizequeryandroid.SizeQuery
import com.usizy.sizequeryandroid.SizeQueryException
import com.usizy.sizequeryandroid.model.DisplayButtonResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import kotlin.coroutines.CoroutineContext


class UsizyButton constructor(
    ctx: Context,
    attrs: AttributeSet?
) : AppCompatButton(ctx, attrs), CoroutineScope {

    private val job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var ctx: Context = ctx
    private var listener: UsizyButtonListenerAdapter? = null

    private var productId: String? = null
    private var logoResId: Int? = null
    private var iconResId: Int? = null
    private var user: String? = null

    constructor(ctx: Context) : this(ctx, null)

    init {
        job = Job()

        initializeIcon()
        initializeText(attrs)
        initializeAppearance(attrs)
        initializeId(attrs)
        addOnClickListener()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        registerEvents()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        unRegisterEvents()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: RecommendSizeEvent?) {
        if (event?.productId.equals(productId)) {
            text = event?.response?.usizySize
            listener?.onRecommendedSize(event?.productId, event?.response)

            SizeCache.getInstance(ctx).cacheRecommend[productId!!] = event?.response?.usizySize!!
        }
    }

    private fun initializeIcon() {
        val icon = if (iconResId != null) {
            resources.getDrawable(iconResId!!, null)
        } else {
            resources.getDrawable(R.drawable.ic_usizy_button, null)
        }
        setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null)
    }

    private fun initializeText(attrs: AttributeSet?) {
        if (attrs?.getAttributeValue(
                ButtonAttributes.ATTRS_NAMESPACE,
                ButtonAttributes.TEXT
            ) == null
        ) {
            text = resources.getString(R.string.buttons_recommendme)
        }
    }

    private fun initializeAppearance(attrs: AttributeSet?) {
        includeFontPadding = true
        visibility = View.INVISIBLE
        compoundDrawablePadding = 20

        if (attrs?.getAttributeValue(
                ButtonAttributes.ATTRS_NAMESPACE,
                ButtonAttributes.TEXT_COLOR
            ) == null
        ) {
            setTextColor(ContextCompat.getColor(ctx, R.color.text_color_selector))
        }
        if (attrs?.getAttributeValue(
                ButtonAttributes.ATTRS_NAMESPACE,
                ButtonAttributes.BACKGROUND
            ) == null
        ) {
            setBackgroundColor(Color.TRANSPARENT)
        }
        if (attrs?.getAttributeValue(
                ButtonAttributes.ATTRS_NAMESPACE,
                ButtonAttributes.TEXT_ALL_CAPS
            ) == null
        ) {
            setSupportAllCaps(false)
        }
    }

    private fun addOnClickListener() {
        setOnClickListener {

            if (productId != null) {
                startFormActivity()
            }
        }
    }

    private fun initializeId(attrs: AttributeSet?) {
        if (attrs?.getAttributeValue(
                ButtonAttributes.ATTRS_NAMESPACE,
                ButtonAttributes.ID
            ) == null
        ) {
            id = generateViewId()
        }
    }

    private fun startFormActivity() {
        val intent = Intent(ctx, FormActivity::class.java)

        if (productId != null) {
            intent.putExtra(PRODUCT_DATA_KEY, productId)
        }

        if (logoResId != null) {
            intent.putExtra(LOGO_RES_ID_KEY, logoResId)
        }

        if (user != null) {
            intent.putExtra(USER_RES_ID_KEY, user)
        }

        ctx.startActivity(intent)
    }

    private fun registerEvents() {
        EventBus.getDefault().register(this)
    }

    private fun unRegisterEvents() {
        EventBus.getDefault().unregister(this)
    }

    private fun updateConfiguration(configuration: UsizyButtonConfiguration) {
        productId = configuration.productId
        logoResId = configuration.logoResId
        iconResId = configuration.iconResId
        user = configuration.user

        if (iconResId != null) {
            initializeIcon()
        }
    }

    private fun fetchDisplayButton() {
        if (productId == null || user == null) return
        launch {
            val cache = SizeCache.getInstance(ctx)
            try {
                val requester = SizeQuery(ctx, user!!)
                val response: DisplayButtonResponse = requester.displayButton(productId!!)

                cache.cacheDisplay[productId!!] = response.show!!
                if (cache.cacheSK == null || cache.cacheSK.isNullOrEmpty()) {
                    cache.cacheSK = response.sk!!
                }

                visibility = if (response.show == ShowProduct.APP) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
            } catch (e: SizeQueryException) {
                cache.cacheDisplay[productId!!] = ShowProduct.ERROR
                visibility = View.INVISIBLE
            }
        }
    }

    fun initialize(configuration: UsizyButtonConfiguration) {
        updateConfiguration(configuration)

        val recommend = SizeCache.getInstance(ctx).cacheRecommend[productId]
        if (recommend != null && recommend.isNotEmpty()) {
            text = recommend
            visibility = View.VISIBLE
            return
        }

        val display = SizeCache.getInstance(ctx).cacheDisplay[productId]
        if (display != null && display.isNotEmpty() && display == ShowProduct.APP) {
            visibility = View.VISIBLE
            return
        } else if (display != null && display.isNotEmpty()) {
            visibility = View.GONE
            return
        }
        fetchDisplayButton()
    }

    fun setOnSizeListener(listener: UsizyButtonListenerAdapter) {
        this.listener = listener
    }
}