package com.usizy.sizebutton

import android.content.Context
import com.usizy.sizequeryandroid.SizeQuery
import com.usizy.sizequeryandroid.model.ConfirmRequest
import com.usizy.sizequeryandroid.model.ConfirmResponseErrors


class UsizyApi {
    companion object {
        @JvmStatic
        suspend fun confirm(context: Context, request: ConfirmRequest): ConfirmResponseErrors? {
            val requester = SizeQuery(context)
            return requester.confirm(request)
        }
    }
}
