package com.usizy.sizebutton.constants

const val PRODUCT_DATA_KEY: String = "PRODUCT_DATA_KEY"
const val LOGO_RES_ID_KEY: String = "LOGO_RES_ID_KEY"
const val USER_RES_ID_KEY: String = "USER_RES_ID_KEY"
