package com.usizy.sizebutton.constants

/**
 * See https://developer.android.com/reference/android/widget/Button for complete list
 * of Button Attrs
 */

object ButtonAttributes {
    const val ATTRS_NAMESPACE = "http://schemas.android.com/apk/res/android"
    const val TEXT_COLOR = "textColor"
    const val TEXT_SIZE = "textSize"
    const val TEXT = "text"
    const val TEXT_STYLE = "textStyle"
    const val BACKGROUND = "background"
    const val PADDING = "padding"
    const val ID = "id"
    const val TEXT_ALL_CAPS = "textAllCaps"
}

/*
android:id	It is used to uniquely identify the control
android:gravity	It is used to specify how to align the text like left, right, center, top, etc.
android:text	It is used to set the text.
android:textColor	It is used to change the color of text.
android:textSize	It is used to specify the size of the text.
android:textStyle	It is used to change the style (bold, italic, bolditalic) of text.
android:background	It is used to set the background color for button control.
android:padding	It is used to set the padding from left, right, top and bottom.
android:drawableBottom	It’s drawable to be drawn to the below of text.
android:drawableRight	It’s drawable to be drawn to the right of text.
android:drawableLeft
 */
