package com.usizy.sizebutton.constants


const val SEEK_BAR_MAX_VALUE = 4
const val SEEK_BAR_STEP_VALUE = 1
const val SEEK_BAR_MIN_VALUE = 0
const val SEEK_BAR_DEFAULT_VALUE = 1
const val METRIC_VALUE = "metric"
const val IMPERIAL_VALUE = "imperial"