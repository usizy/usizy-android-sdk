package com.usizy.sizebutton.cache

import android.content.Context
import android.content.SharedPreferences

class SizeCache private constructor(context: Context) {

    private val PREFS_NAME = "com.usizy.sharedpreferences"
    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_NAME, 0)

    val cacheDisplay = HashMap<String, String>()
    val cacheRecommend = HashMap<String, String>()

    var cacheSK:String? = null
        set(value) {
            field = value
            prefs.edit().putString("sk", value).apply()
        }

    init {
        cacheSK = prefs.getString("sk", "")
    }


    companion object : SingletonHolder<SizeCache, Context>(::SizeCache)
}

open class SingletonHolder<out T: Any, in A>(creator: (A) -> T) {
    private var creator: ((A) -> T)? = creator
    @Volatile private var instance: T? = null

    fun getInstance(arg: A): T {
        val i = instance
        if (i != null) {
            return i
        }

        return synchronized(this) {
            val i2 = instance
            if (i2 != null) {
                i2
            } else {
                val created = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }
}