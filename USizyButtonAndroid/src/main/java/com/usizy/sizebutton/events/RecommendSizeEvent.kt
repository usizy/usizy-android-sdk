package com.usizy.sizebutton.events

import com.usizy.sizequeryandroid.model.SaveRecommendResponse

class RecommendSizeEvent {
    var productId: String? = null
    var response: SaveRecommendResponse? = null
}