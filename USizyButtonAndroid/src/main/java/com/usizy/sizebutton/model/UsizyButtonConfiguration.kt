package com.usizy.sizebutton.model

class UsizyButtonConfiguration {
    var productId: String? = null
    var logoResId: Int? = null
    var iconResId: Int? = null
    var user: String? = null
}