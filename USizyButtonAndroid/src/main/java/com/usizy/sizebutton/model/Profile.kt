package com.usizy.sizebutton.model

import com.usizy.sizequeryandroid.model.HeightImperial

open class Profile {
    var metricHeight: Float? = null
    var imperialHeight: HeightImperial? = null
    var weightMet: Float? = null
    var weightImp: Float? = null
    var chestMet: Float? = null
    var chestImp: Float? = null
    var waistMet: Float? = null
    var waistImp: Float? = null
    var hipsMet: Float? = null
    var hipsImp: Float? = null
    var fit: Int? = null
    var age: Int? = null
    var chest_type: Int? = null
    var waist_type: Int? = null
    var hips_type: Int? = null
}