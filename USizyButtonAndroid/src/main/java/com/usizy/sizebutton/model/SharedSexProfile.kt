package com.usizy.sizebutton.model

import com.usizy.sizequeryandroid.SexValues
import com.usizy.sizequeryandroid.model.HeightImperial

object SharedSexProfile {
    var selectedSex: String? = null
//    var isFromAdvanced: Bool
//    var isImperial: Bool

    var profileMale= ProfileMale()
    var profileFemale= ProfileFemale()
    var profileBoy = ProfileBoy()
    var profileStandar = ProfileStandar()

//    init {
//        selectedSex = null
//
//        profileBoy = ProfileBoy()
//        profileMale = ProfileMale()
//        profileFemale = ProfileFemale()
//        profileStandar = ProfileStandar()
//    }

    fun clearValues() {
//        isImperial = false
//        isFromAdvanced = false
        selectedSex = null

        profileBoy = ProfileBoy()
        profileMale = ProfileMale()
        profileFemale = ProfileFemale()
        profileStandar = ProfileStandar()
    }

    fun getProfile(sex: String?): Profile {
        var profile: Profile
        when (sex) {
            SexValues.UNISEX -> {
                if (selectedSex == SexValues.MALE) {
                    profile = this.profileMale
                } else {
                    profile = this.profileFemale
                }
            }
            SexValues.HUMAN -> {
                if (selectedSex == SexValues.MALE) {
                    profile = this.profileMale
                } else {
                    profile = this.profileBoy
                }
            }
            else -> profile = this.profileStandar
        }
        return profile
    }


}
