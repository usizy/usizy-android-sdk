package com.usizy.sizebutton.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.usizy.sizebutton.R
import com.usizy.sizebutton.utils.ShoeStringHelper

class ShoeSizesAdapter(list: ArrayList<ArrayList<*>>) : BaseAdapter() {
    var sizesList = ShoeStringHelper.formatUnicodeCharsList(list)

    override fun getView(position: Int, convertView: View?, container: ViewGroup?): View {
        val inflater = container?.context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.size_item_cell, null)
        val tvSizeText = view.findViewById<TextView>(R.id.tv_size_item)

        tvSizeText.text = sizesList[position]
        return view
    }

    override fun getCount(): Int {
        return sizesList.size
    }

    override fun getItem(position: Int): String {
        return sizesList[position]
    }

    fun update(data: ArrayList<ArrayList<*>>) {
        sizesList = ShoeStringHelper.formatUnicodeCharsList(data)
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return 0
    }
}