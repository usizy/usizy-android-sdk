package com.usizy.sizebutton.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.widget.doAfterTextChanged
import androidx.core.widget.doBeforeTextChanged
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.usizy.sizebutton.R
import com.usizy.sizebutton.constants.*
import com.usizy.sizebutton.model.Profile
import com.usizy.sizebutton.model.SharedSexProfile
import com.usizy.sizebutton.utils.DataExtractionUtils
import com.usizy.sizebutton.utils.FormValidationHelpers
import com.usizy.sizebutton.utils.InputShape
import com.usizy.sizebutton.utils.MetricsConvertHelpers
import com.usizy.sizequeryandroid.MeasurementsValues
import com.usizy.sizequeryandroid.MetricSystem
import com.usizy.sizequeryandroid.RecommendationWay
import com.usizy.sizequeryandroid.SexValues
import com.usizy.sizequeryandroid.model.*
import kotlin.math.floor
import kotlin.math.roundToInt

private const val ARG_PRODUCT = "ARG_PRODUCT"
private const val ARG_DATA = "ARG_DATA"
private const val ARG_USER = "ARG_USER"
private const val ARG_SELECTED_SEX = "ARG_SELECTED_SEX"

/**
 * A simple [Fragment] subclass.
 * Use the [YourMeasuresFormFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class YourMeasuresFormFragment : Fragment() {
    private var product: String? = null
    private var data: PopupData? = null
    private var user: String? = null

    private lateinit var measuresList: List<String>
    private lateinit var limitsMetric: LimitsMetric
    private lateinit var limitsImperial: LimitsImperial

    private var subjectDataMetric: SubjectData? = null
    private var subjectDataImperial: SubjectDataImperial? = null
    private var selectedSex: String? = null
    private var isMetric: Boolean = true
    private var imageProductURL: String? = null

    private lateinit var inputsList: MutableCollection<InputShape>

    private var minInchesHeight: Float = 0f
    private var maxInchesHeight: Float = 0f
    private var currentInchesHeight: Int = 0

    private var heightCm: Float = 0f
    private var heightFeet: Float = 0f
    private var heightInches: Float = 0f
    private var heightTotalInches: Float = 0f
    private var chestCm: Float = 0f
    private var chestInches: Float = 0f
    private var waistCm: Float = 0f
    private var waistInches: Float = 0f
    private var hipsCm: Float = 0f
    private var hipsInches: Float = 0f
    private var sexProfile: Profile? = null

    private var init = true

    private lateinit var tvBackToBasic: TextView
    private lateinit var formMeasuresMetricView: View
    private lateinit var formMeasuresImperialView: View
    private lateinit var sbFitLooseControl: SeekBar
    private lateinit var tgBtnCmIn: ToggleButton
    private lateinit var btnFitter: Button
    private lateinit var btnLooser: Button
    private lateinit var btnContinue: Button

    private lateinit var llHeightInputMetric: LinearLayout
    private lateinit var llChestInputMetric: LinearLayout
    private lateinit var llWaistInputMetric: LinearLayout
    private lateinit var llHipsInputMetric: LinearLayout

    private lateinit var llHeightInputImperial: LinearLayout
    private lateinit var llChestInputImperial: LinearLayout
    private lateinit var llWaistInputImperial: LinearLayout
    private lateinit var llHipsInputImperial: LinearLayout

    private lateinit var tilHeightMetric: TextInputLayout
    private lateinit var tiEtHeightMetric: TextInputEditText
    private lateinit var tilChestMetric: TextInputLayout
    private lateinit var tiEtChestMetric: TextInputEditText
    private lateinit var tilWaistMetric: TextInputLayout
    private lateinit var tiEtWaistMetric: TextInputEditText
    private lateinit var tilHipsMetric: TextInputLayout
    private lateinit var tiEtHipsMetric: TextInputEditText

    private lateinit var tilHeightFeetImperial: TextInputLayout
    private lateinit var tiEtHeightFeetImperial: TextInputEditText
    private lateinit var tilHeightInchesImperial: TextInputLayout
    private lateinit var tiEtHeightInchesImperial: TextInputEditText
    private lateinit var tilChestImperial: TextInputLayout
    private lateinit var tiEtChestImperial: TextInputEditText
    private lateinit var tilWaistImperial: TextInputLayout
    private lateinit var tiEtWaistImperial: TextInputEditText
    private lateinit var tilHipsImperial: TextInputLayout
    private lateinit var tiEtHipsImperial: TextInputEditText
    private lateinit var ivProductImage: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            product = it.getString(ARG_PRODUCT)
            data = Gson().fromJson(it.getString(ARG_DATA), PopupData::class.java)
            user = it.getString(ARG_USER)
            if (!it.getString(ARG_SELECTED_SEX).isNullOrBlank()) {
                selectedSex = it.getString(ARG_SELECTED_SEX)
            }
        }

        measuresList = data?.measurements!!
        limitsImperial = data?.sexLimits!!.imperial
        limitsMetric = data?.sexLimits!!.metric
        inputsList = mutableListOf()
        updateSubjectData()

        minInchesHeight = limitsImperial.height.min.ft * 12 + limitsImperial.height.min.inX
        maxInchesHeight = limitsImperial.height.max.ft * 12 + limitsImperial.height.max.inX

        currentInchesHeight = (subjectDataImperial?.height!!.ft * 12 + subjectDataImperial?.height!!.inX).toInt()
        imageProductURL = data?.image

        initSelectedSex()
        initMeasuresValues()
    }

    private fun initMeasuresValues() {
        heightCm = sexProfile?.metricHeight ?: subjectDataMetric?.height!!
        heightFeet = sexProfile?.imperialHeight?.ft ?: subjectDataImperial?.height!!.ft
        heightInches = sexProfile?.imperialHeight?.inX ?: subjectDataImperial?.height!!.inX
        heightTotalInches = heightFeet * 12 + heightInches

        chestCm = sexProfile?.chestMet ?: subjectDataMetric?.chest!!
        chestInches = sexProfile?.chestImp ?: subjectDataImperial?.chest!!
        waistCm = sexProfile?.waistMet ?: subjectDataMetric?.waist!!
        waistInches = sexProfile?.waistImp ?: subjectDataImperial?.waist!!
        hipsCm = sexProfile?.hipsMet ?: subjectDataMetric?.hips!!.toFloat()
        hipsInches = sexProfile?.hipsImp ?: subjectDataImperial?.hips!!.toFloat()

        persistSharedData()
    }

    private fun initSelectedSex() {
        if (data?.sex == SexValues.UNISEX) {
            selectedSex = SharedSexProfile.selectedSex ?: SexValues.MALE
            SharedSexProfile.selectedSex = selectedSex
        } else if (data?.sex == SexValues.HUMAN) {
            selectedSex = SharedSexProfile.selectedSex ?: SexValues.BOY
            SharedSexProfile.selectedSex = selectedSex
        } else {
            selectedSex = null
            SharedSexProfile.selectedSex = null
        }
        sexProfile = SharedSexProfile.getProfile(data?.sex)
    }

    private fun persistSharedData() {
        sexProfile?.metricHeight = heightCm
        sexProfile?.imperialHeight = HeightImperial(heightFeet, heightInches)

        sexProfile?.chestMet = chestCm
        sexProfile?.chestImp = chestInches
        sexProfile?.waistMet = waistCm
        sexProfile?.waistImp = waistInches
        sexProfile?.hipsMet = hipsCm
        sexProfile?.hipsImp = hipsInches
    }

    private fun updateSharedProfile(){
        if (isMetric){
            if (!tiEtHeightMetric.text.toString().isNullOrEmpty()){
                heightCm =  tiEtHeightMetric.text.toString().toFloat()
            }
            if (!tiEtChestMetric.text.toString().isNullOrEmpty()){
                chestCm =  tiEtChestMetric.text.toString().toFloat()
            }
            if (!tiEtWaistMetric.text.toString().isNullOrEmpty()){
                waistCm =  tiEtWaistMetric.text.toString().toFloat()
            }
            if (!tiEtHipsMetric.text.toString().isNullOrEmpty()){
                hipsCm =  tiEtHipsMetric.text.toString().toFloat()
            }
        }else{
            if (!tiEtHeightFeetImperial.text.toString().isNullOrEmpty()){
                heightFeet = tiEtHeightFeetImperial.text.toString().toFloat()
            }
            if (!tiEtHeightInchesImperial.text.toString().isNullOrEmpty()){
                heightInches = tiEtHeightInchesImperial.text.toString().toFloat()
            }
            if (!tiEtChestImperial.text.toString().isNullOrEmpty()){
                chestInches = tiEtChestImperial.text.toString().toFloat()
            }
            if (!tiEtWaistImperial.text.toString().isNullOrEmpty()){
                waistInches = tiEtWaistImperial.text.toString().toFloat()
            }
            if (!tiEtHipsImperial.text.toString().isNullOrEmpty()){
                hipsInches = tiEtHipsImperial.text.toString().toFloat()
            }
        }

        persistSharedData()
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_your_measures_form, container, false)
        initViews(rootView)
        setOnClickListeners()
        initSeekBar()
        handleInputsLoad()
        initInputsListeners()
        setImperialHeightInputsListeners()
        setDefaultFormValues()

        init = false
        return rootView
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param product productId.
         * @param user user string
         * @param data PopupData
         * @param selectedSex sex string
         * @return A new instance of fragment YourMeasuresFormFragment.
         */
        @JvmStatic
        fun newInstance(product: String, user: String, data: PopupData, selectedSex: String) =
            YourMeasuresFormFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PRODUCT, product)
                    putString(ARG_DATA, Gson().toJson(data))
                    putString(ARG_USER, user)
                    putString(ARG_SELECTED_SEX, selectedSex)
                }
            }
    }

    private fun initViews(it: View) {
        sbFitLooseControl = it.findViewById(R.id.sb_fit_loose_control)
        btnFitter = it.findViewById(R.id.btn_fit_controller)
        btnLooser = it.findViewById(R.id.btn_loose_controller)
        tgBtnCmIn = it.findViewById(R.id.toggle_cm_in)
        tvBackToBasic = it.findViewById(R.id.tv_to_basic_measures)
        btnContinue = it.findViewById(R.id.btn_measures_continue)
        ivProductImage = it.findViewById(R.id.iv_product_measures)

        initSexRadioGroup(it)
        initMetricViews(it)
        initImperialViews(it)
        initMetricInputs(it)
        initImperialInputs(it)
        initImageView()
    }

    private fun initImageView() {
        if (!imageProductURL.isNullOrBlank()) {
            Glide.with(this).load(imageProductURL).into(ivProductImage)
            ivProductImage.visibility = View.VISIBLE
        }
    }

    private fun initMetricViews(it: View) {
        formMeasuresMetricView = it.findViewById(R.id.form_measures_metric_inputs)

        llHeightInputMetric = it.findViewById(R.id.ll_height_input_metric)
        llChestInputMetric = it.findViewById(R.id.ll_chest_input_metric)
        llWaistInputMetric = it.findViewById(R.id.ll_waist_input_metric)
        llHipsInputMetric = it.findViewById(R.id.ll_hips_input_metric)
    }

    private fun initImperialViews(it: View) {
        formMeasuresImperialView = it.findViewById(R.id.form_measures_imperial_inputs)

        llHeightInputImperial = it.findViewById(R.id.ll_height_input_imperial)
        llChestInputImperial = it.findViewById(R.id.ll_chest_input_imperial)
        llWaistInputImperial = it.findViewById(R.id.ll_waist_input_imperial)
        llHipsInputImperial = it.findViewById(R.id.ll_hips_input_imperial)
    }

    private fun initInputsListeners() {
        for (input in inputsList) {
            if(!input.skipListener) setOnChangeTextListeners(input.container!!, input.input!!, input.max, input.min)
        }
    }

    private fun handleInputsLoad() {
        hideAllInputsLayouts()
        if (!measuresList.isNullOrEmpty()) {
            if (measuresList.contains(MeasurementsValues.HEIGHT)) {
                inputsList.add(
                        InputShape(
                                container = tilHeightMetric, input = tiEtHeightMetric,
                                max = limitsMetric.height.max, min = limitsMetric.height.min,
                                type = METRIC_VALUE, isInputFormHolderVisible = isMetric,
                                inputVisible = llHeightInputMetric.visibility
                        )
                )
                inputsList.add(
                        InputShape(
                                container = tilHeightFeetImperial, input = tiEtHeightFeetImperial,
                                max = limitsImperial.height.max.ft, min = limitsImperial.height.min.ft,
                                type = IMPERIAL_VALUE, isInputFormHolderVisible = !isMetric,
                                inputVisible = llHeightInputImperial.visibility,
                                skipListener = true, isImperialHeight = true
                        )
                )
                inputsList.add(
                        InputShape(
                                container = tilHeightInchesImperial, input = tiEtHeightInchesImperial,
                                max = limitsImperial.height.max.inX, min = limitsImperial.height.min.inX,
                                type = IMPERIAL_VALUE, isInputFormHolderVisible = !isMetric,
                                inputVisible = llHeightInputImperial.visibility,
                                skipListener = true, isImperialHeight = true
                        )
                )

                setHeightLayoutVisibility(View.VISIBLE)
            }
            if (measuresList.contains(MeasurementsValues.WAIST)) {
                inputsList.add(
                        InputShape(
                                container = tilWaistMetric, input = tiEtWaistMetric,
                                max = limitsMetric.waist.max, min = limitsMetric.waist.min,
                                type = METRIC_VALUE, isInputFormHolderVisible = isMetric,
                                inputVisible = llWaistInputMetric.visibility
                        )
                )
                inputsList.add(
                        InputShape(
                                container = tilWaistImperial, input = tiEtWaistImperial,
                                max = limitsImperial.waist.max, min = limitsImperial.waist.min,
                                type = IMPERIAL_VALUE, isInputFormHolderVisible = !isMetric,
                                inputVisible = llWaistInputImperial.visibility
                        )
                )

                setWaistLayoutVisibility(View.VISIBLE)
            }
            if (measuresList.contains(MeasurementsValues.CHEST)) {
                inputsList.add(
                        InputShape(
                                container = tilChestMetric, input = tiEtChestMetric,
                                max = limitsMetric.chest.max, min = limitsMetric.chest.min,
                                type = METRIC_VALUE, isInputFormHolderVisible = isMetric,
                                inputVisible = llChestInputMetric.visibility
                        )
                )
                inputsList.add(
                        InputShape(
                                container = tilChestImperial, input = tiEtChestImperial,
                                max = limitsImperial.chest.max, min = limitsImperial.chest.min,
                                type = IMPERIAL_VALUE, isInputFormHolderVisible = !isMetric,
                                inputVisible = llChestInputImperial.visibility
                        )
                )

                setChestLayoutVisibility(View.VISIBLE)
            }
            if (measuresList.contains(MeasurementsValues.HIPS)) {
                inputsList.add(
                        InputShape(
                                container = tilHipsMetric, input = tiEtHipsMetric,
                                max = limitsMetric.hips.max, min = limitsMetric.hips.min,
                                type = METRIC_VALUE, isInputFormHolderVisible = isMetric,
                                inputVisible = llHipsInputMetric.visibility
                        )
                )
                inputsList.add(
                        InputShape(
                                container = tilHipsImperial, input = tiEtHipsImperial,
                                max = limitsImperial.hips.max, min = limitsImperial.hips.min,
                                type = IMPERIAL_VALUE, isInputFormHolderVisible = !isMetric,
                                inputVisible = llHipsInputImperial.visibility
                        )
                )

                setHipsLayoutVisibility(View.VISIBLE)
            }
        }
    }

    private fun setDefaultFormValues() {
        tiEtHeightMetric.setText(heightCm.roundToInt().toString())
        tiEtHeightFeetImperial.setText(floor(heightFeet).toInt().toString())
        tiEtHeightInchesImperial.setText(floor(heightInches).toInt().toString())
        tiEtWaistMetric.setText(floor(waistCm).toInt().toString())
        tiEtWaistImperial.setText(floor(waistInches).toInt().toString())
        tiEtChestMetric.setText(floor(chestCm).toInt().toString())
        tiEtChestImperial.setText(floor(chestInches).toInt().toString())
        tiEtHipsMetric.setText(floor(hipsCm).toInt().toString())
        tiEtHipsImperial.setText(floor(hipsInches).toInt().toString())
    }

    private fun setImperialHeightInputsListeners() {
        // FEET
        var intPartFt: Int
        var decPartFt = 0f
        tiEtHeightFeetImperial.doOnTextChanged { text, _, _, _ ->
            manageHeightFeetErrorMessage(text = text.toString())
        }

        tiEtHeightFeetImperial.doBeforeTextChanged { _, _, _, _ ->
            intPartFt = heightFeet.toInt()
            decPartFt = heightFeet - intPartFt
        }

        tiEtHeightFeetImperial.doAfterTextChanged {
            manageHeightFeetErrorMessage(text = it.toString())
            if (!it.toString().isNullOrBlank()) {
                intPartFt = it.toString().toInt()
                heightFeet = intPartFt + decPartFt
            }
        }

        // INCHES
        var intPartIn: Int
        var decPartIn = 0f

        tiEtHeightInchesImperial.doOnTextChanged { text, _, _, _ ->
            manageHeightInchesErrorMessage(text = text.toString())
        }

        tiEtHeightInchesImperial.doBeforeTextChanged { _, _, _, _ ->
            intPartIn = heightInches.toInt()
            decPartIn = heightInches - intPartIn
        }

        tiEtHeightInchesImperial.doAfterTextChanged {
            manageHeightInchesErrorMessage(text = it.toString())
            if (!it.toString().isNullOrBlank()) {
                intPartIn = it.toString().toInt()
                heightInches = intPartIn + decPartIn
            }
        }

        tiEtHeightFeetImperial.setOnFocusChangeListener{it, hasFocus ->
            if (!hasFocus) {
                val text = (it as TextInputEditText).text.toString()
                manageHeightFeetErrorMessage(text)
            }
        }

        tiEtHeightInchesImperial.setOnFocusChangeListener{it, hasFocus ->
            if (!hasFocus) {
                val text = (it as TextInputEditText).text.toString()
                manageHeightInchesErrorMessage(text)
            }
        }
    }

    private fun manageHeightInchesErrorMessage(text: String): Boolean {
        setCurrentInches(tiEtHeightFeetImperial.text.toString(), text)
        var hasError = false
        if (text.isNullOrEmpty() || currentInchesHeight < minInchesHeight || currentInchesHeight > maxInchesHeight || Integer.parseInt(
                        text
                ) > 11) {
            if (!init) {
                setInputError(
                        tilHeightInchesImperial,
                        "${limitsImperial.height.min.ft.roundToInt()} ft. ${limitsImperial.height.min.inX.roundToInt()} in. - ${limitsImperial.height.max.ft.roundToInt()} ft. ${limitsImperial.height.max.inX.roundToInt()} in."
                )
                setInputError(
                        tilHeightFeetImperial,
                        "${limitsImperial.height.min.ft.roundToInt()} ft. ${limitsImperial.height.min.inX.roundToInt()} in. - ${limitsImperial.height.max.ft.roundToInt()} ft. ${limitsImperial.height.max.inX.roundToInt()} in."
                )
                hasError = true
            }
        } else {
            setInputError(tilHeightInchesImperial, "")
            setInputError(tilHeightFeetImperial, "")
        }
        return hasError
    }

    private fun manageHeightFeetErrorMessage(text: String): Boolean {
        setCurrentInches(text, tiEtHeightInchesImperial.text.toString())
        var hasError = false
        if (text.isNullOrEmpty() || currentInchesHeight < minInchesHeight || currentInchesHeight > maxInchesHeight) {
            if(!init) {
                setInputError(
                        tilHeightFeetImperial,
                        "${limitsImperial.height.min.ft.roundToInt()} ft. ${limitsImperial.height.min.inX.roundToInt()} in. - ${limitsImperial.height.max.ft.roundToInt()} ft. ${limitsImperial.height.max.inX.roundToInt()} in."
                )
                setInputError(
                        tilHeightInchesImperial,
                        "${limitsImperial.height.min.ft.roundToInt()} ft. ${limitsImperial.height.min.inX.roundToInt()} in. - ${limitsImperial.height.max.ft.roundToInt()} ft. ${limitsImperial.height.max.inX.roundToInt()} in."
                )
                hasError = true
            }
        } else {
            setInputError(tilHeightFeetImperial, "")
            setInputError(tilHeightInchesImperial, "")
        }
        return hasError
    }

    private fun setCurrentInches(feet: String, inches: String) {
        if (!feet.isNullOrEmpty() && !inches.isNullOrEmpty()) {
            currentInchesHeight = Integer.parseInt(feet) * 12 + Integer.parseInt(inches)
        }
    }

    private fun hideAllInputsLayouts() {
        setHeightLayoutVisibility(View.GONE)
        setChestLayoutVisibility(View.GONE)
        setWaistLayoutVisibility(View.GONE)
        setHipsLayoutVisibility(View.GONE)
    }

    private fun setHeightLayoutVisibility(visibility: Int) {
        llHeightInputMetric.visibility = visibility
        llHeightInputImperial.visibility = visibility
    }

    private fun setChestLayoutVisibility(visibility: Int) {
        llChestInputMetric.visibility = visibility
        llChestInputImperial.visibility = visibility
    }

    private fun setWaistLayoutVisibility(visibility: Int) {
        llWaistInputMetric.visibility = visibility
        llWaistInputMetric.visibility = visibility
    }

    private fun setHipsLayoutVisibility(visibility: Int) {
        llHipsInputMetric.visibility = visibility
        llHipsInputImperial.visibility = visibility
    }

    private fun initMetricInputs(it: View) {
        tilHeightMetric = it.findViewById(R.id.til_height_measures_metric)
        tiEtHeightMetric = it.findViewById(R.id.ti_et_height_measures_metric)
        tiEtHeightMetric.setSelectAllOnFocus(true)

        tilChestMetric = it.findViewById(R.id.til_chest_measures_metric)
        tiEtChestMetric = it.findViewById(R.id.ti_et_chest_measures_metric)
        tiEtChestMetric.setSelectAllOnFocus(true)

        tilWaistMetric = it.findViewById(R.id.til_waist_measures_metric)
        tiEtWaistMetric = it.findViewById(R.id.ti_et_waist_measures_metric)
        tiEtWaistMetric.setSelectAllOnFocus(true)

        tilHipsMetric = it.findViewById(R.id.til_hips_measures_metric)
        tiEtHipsMetric = it.findViewById(R.id.ti_et_hips_measures_metric)
        tiEtHipsMetric.setSelectAllOnFocus(true)
    }

    private fun initImperialInputs(it: View) {
        tilHeightFeetImperial = it.findViewById(R.id.til_measures_feet)
        tiEtHeightFeetImperial = it.findViewById(R.id.ti_et_measures_feet)
        tiEtHeightFeetImperial.setSelectAllOnFocus(true)

        tilHeightInchesImperial = it.findViewById(R.id.til_measures_inches)
        tiEtHeightInchesImperial = it.findViewById(R.id.ti_et_measures_inches)
        tiEtHeightInchesImperial.setSelectAllOnFocus(true)

        tilChestImperial = it.findViewById(R.id.til_chest_measures_imperial)
        tiEtChestImperial = it.findViewById(R.id.ti_et_chest_measures_imperial)
        tiEtChestImperial.setSelectAllOnFocus(true)

        tilWaistImperial = it.findViewById(R.id.til_waist_measures_imperial)
        tiEtWaistImperial = it.findViewById(R.id.ti_et_waist_measures_imperial)
        tiEtWaistImperial.setSelectAllOnFocus(true)

        tilHipsImperial = it.findViewById(R.id.til_hips_measures_imperial)
        tiEtHipsImperial = it.findViewById(R.id.ti_et_hips_measures_imperial)
        tiEtHipsImperial.setSelectAllOnFocus(true)
    }

    private fun initSexRadioGroup(it: View) {
        if (data?.sex == SexValues.UNISEX){
            val iSex = it.findViewById<View>(R.id.iSex)
            iSex.visibility = View.VISIBLE

            val rgSex = it.findViewById<RadioGroup>(R.id.rgSex)
            if (!selectedSex.isNullOrEmpty()) {
                when (selectedSex) {
                    SexValues.MALE -> rgSex.check(R.id.male)
                    SexValues.FEMALE -> rgSex.check(R.id.female)
                }
            } else {
                rgSex.check(R.id.male)
                selectedSex = SexValues.MALE
                SharedSexProfile.selectedSex = selectedSex
            }
            rgSex.setOnCheckedChangeListener { _, checkedId ->
                selectedSex = if (checkedId == R.id.male) SexValues.MALE else SexValues.FEMALE
                SharedSexProfile.selectedSex = selectedSex
                sexProfile = SharedSexProfile.getProfile(data?.sex)
                updateDataAndViewsOnSexChange()
            }
        }else if (data?.sex == SexValues.HUMAN){
            val iHuman = it.findViewById<View>(R.id.iHuman)
            iHuman.visibility = View.VISIBLE

            val rgHuman = it.findViewById<RadioGroup>(R.id.rgHuman)

            if (!selectedSex.isNullOrEmpty()) {
                when (selectedSex) {
                    SexValues.MALE -> rgHuman.check(R.id.adult)
                    SexValues.BOY -> rgHuman.check(R.id.child)
                }
            } else {
                rgHuman.check(R.id.adult)
                selectedSex = SexValues.MALE
                SharedSexProfile.selectedSex = selectedSex
                sexProfile = SharedSexProfile.getProfile(data?.sex)
                SharedSexProfile.selectedSex = selectedSex
            }

            rgHuman.setOnCheckedChangeListener { _, checkedId ->
                selectedSex = if (checkedId == R.id.adult) SexValues.MALE else SexValues.BOY
                updateDataAndViewsOnSexChange()
            }
        }else{
            selectedSex = data?.sex
        }
        sexProfile = SharedSexProfile.getProfile(data?.sex)
    }

    private fun initSeekBar() {
        sbFitLooseControl.progress = subjectDataMetric!!.fit - 1
        sbFitLooseControl.max = SEEK_BAR_MAX_VALUE
        sbFitLooseControl.incrementProgressBy(SEEK_BAR_STEP_VALUE)
        setOnChangeSeekBarListener()
    }

    private fun setOnClickListeners() {
        /**
         * Navigates to your measures fragment
         * */
        tvBackToBasic.setOnClickListener {
            navigateToMainForm()
        }

        /**
         * Changes -> Decrease -> the value of the fit property in the seekbar
         * */
        btnFitter.setOnClickListener {
            if(sbFitLooseControl.progress < SEEK_BAR_MIN_VALUE) {
                sbFitLooseControl.progress = SEEK_BAR_MIN_VALUE
                return@setOnClickListener
            }
            sbFitLooseControl.progress = sbFitLooseControl.progress - 1
        }

        /**
         * Changes -> Increase -> the value of the fit property in the seekbar
         * */
        btnLooser.setOnClickListener {
            if(sbFitLooseControl.progress > SEEK_BAR_MAX_VALUE) {
                sbFitLooseControl.progress = SEEK_BAR_MAX_VALUE
                return@setOnClickListener
            }
            sbFitLooseControl.progress = sbFitLooseControl.progress + 1
        }

        tgBtnCmIn.setOnCheckedChangeListener { _, checked ->
            if (!checked) {
                isMetric = true
                updateInputsAndValuesOnMetricChange()
                formMeasuresMetricView.visibility = View.VISIBLE
                formMeasuresImperialView.visibility = View.GONE
            } else {
                isMetric = false
                updateInputsAndValuesOnMetricChange()
                formMeasuresMetricView.visibility = View.GONE
                formMeasuresImperialView.visibility = View.VISIBLE
            }
            handleInputsLoad()
        }

        btnContinue.setOnClickListener {
            val filteredInputsByType: List<InputShape>
            val isValidForm: Boolean
            if (isMetric) {
                filteredInputsByType = inputsList.filter { it.type == METRIC_VALUE }
                isValidForm = FormValidationHelpers.isArrayInputsValid(inputsList = filteredInputsByType, checkForVisibleInputs = true)
            } else {
                filteredInputsByType = inputsList.filter { it.type == IMPERIAL_VALUE && !it.isImperialHeight}
                isValidForm = FormValidationHelpers.isArrayInputsValid(inputsList = filteredInputsByType, checkForVisibleInputs = true) && isImperialHeightValid()
            }

            if (isValidForm) {
                navigateToLastStepForm()
            }
        }
    }

    private fun updateInputsAndValuesOnMetricChange() {
        if (isMetric) {
            updateHeightInputsAndValues(type = METRIC_VALUE)
            updateInputsAndValues(type = METRIC_VALUE)
        } else {
            updateHeightInputsAndValues(type = IMPERIAL_VALUE)
            updateInputsAndValues(type = IMPERIAL_VALUE)
        }
    }

    private fun updateHeightInputsAndValues(type: String) {
        when (type) {
            METRIC_VALUE -> {
                heightTotalInches = MetricsConvertHelpers.getTotalInchesFromImperial(HeightImperial(heightFeet, heightInches))
                heightCm = MetricsConvertHelpers.fromFeetInchesToCm(heightFeet, heightInches)
                tiEtHeightMetric.setText(floor(heightCm).toInt().toString())
            }
            IMPERIAL_VALUE -> {
                val inputValue: String? = tiEtHeightMetric.text.toString()
                if (!inputValue.isNullOrBlank()) {
                    heightCm = inputValue.toFloat()
                }
                heightTotalInches = MetricsConvertHelpers.getTotalInchesFromCm(heightCm)
                heightFeet = MetricsConvertHelpers.getImperialFromTotalInches(heightTotalInches).ft
                heightInches = MetricsConvertHelpers.getImperialFromTotalInches(heightTotalInches).inX
                tiEtHeightFeetImperial.setText(floor(heightFeet).toInt().toString())
                tiEtHeightInchesImperial.setText(floor(heightInches).toInt().toString())
            }
        }
    }

    private fun updateInputsAndValues(type: String) {
        when (type) {
            METRIC_VALUE -> {
                val chestValue: String? = tiEtChestImperial.text.toString()
                if (!chestValue.isNullOrBlank()) {
                    val decPart = MetricsConvertHelpers.getDecimalPart(chestInches)
                    chestInches = chestValue.toFloat() + decPart
                }
                chestCm = MetricsConvertHelpers.fromInchesToCm(chestInches)
                tiEtChestMetric.setText(floor(chestCm).toInt().toString())

                val waistValue: String? = tiEtWaistImperial.text.toString()
                if (!waistValue.isNullOrBlank()) {
                    val decPart = MetricsConvertHelpers.getDecimalPart(waistInches)
                    waistInches = waistValue.toFloat() + decPart
                }

                waistCm = MetricsConvertHelpers.fromInchesToCm(waistInches)
                tiEtWaistMetric.setText(floor(waistCm).toInt().toString())

                val hipsValue: String? = tiEtHipsImperial.text.toString()
                if (!hipsValue.isNullOrBlank()) {
                    val decPart = MetricsConvertHelpers.getDecimalPart(hipsInches)
                    hipsInches = hipsValue.toFloat() + decPart
                }

                hipsCm = MetricsConvertHelpers.fromInchesToCm(hipsInches)
                tiEtHipsMetric.setText(floor(hipsCm).toInt().toString())
            }
            IMPERIAL_VALUE -> {
                val chestValue: String? = tiEtChestMetric.text.toString()
                if (!chestValue.isNullOrBlank()) {
                    val decPart = MetricsConvertHelpers.getDecimalPart(chestCm)
                    chestCm = chestValue.toFloat() + decPart
                }
                chestInches = MetricsConvertHelpers.getTotalInchesFromCm(chestCm)
                tiEtChestImperial.setText(floor(chestInches).toInt().toString())

                val waistValue: String? = tiEtWaistMetric.text.toString()
                if (!waistValue.isNullOrBlank()) {
                    val decPart = MetricsConvertHelpers.getDecimalPart(waistCm)
                    waistCm = waistValue.toFloat() + decPart
                }
                waistInches = MetricsConvertHelpers.getTotalInchesFromCm(waistCm)
                tiEtWaistImperial.setText(floor(waistInches).toInt().toString())

                val hipsValue: String? = tiEtHipsMetric.text.toString()
                if (!hipsValue.isNullOrBlank()) {
                    val decPart = MetricsConvertHelpers.getDecimalPart(hipsCm)
                    hipsCm = hipsValue.toFloat() + decPart
                }
                hipsInches = MetricsConvertHelpers.getTotalInchesFromCm(hipsCm)
                tiEtHipsImperial.setText(floor(hipsInches).toInt().toString())
            }
        }
    }

    private fun isImperialHeightValid(): Boolean {
        val feet = tiEtHeightFeetImperial.text.toString()
        val inches = tiEtHeightInchesImperial.text.toString()
        val errorInFeet = manageHeightFeetErrorMessage(feet)
        val errorInInches = manageHeightInchesErrorMessage(inches)
        return !(errorInFeet || errorInInches)
    }

    private fun setOnChangeSeekBarListener() {
        /**
         * SeekBar for fitter control on change listeners
         * */
        sbFitLooseControl.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seek: SeekBar, progress: Int, fromUser: Boolean) {
                btnFitter.isEnabled = progress > SEEK_BAR_MIN_VALUE
                btnLooser.isEnabled = progress < SEEK_BAR_MAX_VALUE
                sexProfile?.fit = progress
            }

            override fun onStartTrackingTouch(seek: SeekBar) {
            }

            override fun onStopTrackingTouch(seek: SeekBar) {
            }
        })
    }

    private fun setOnChangeTextListeners(
            container: TextInputLayout,
            input: TextInputEditText,
            max: Float,
            min: Float
    ) {
        input.doOnTextChanged { text, _, _, _ ->
            if (text.isNullOrEmpty() || Integer.parseInt(text.toString()) < min || Integer.parseInt(
                            text.toString()
                    ) > max
            ) {
               if(!init) setInputError(container, "${min.roundToInt()} - ${max.roundToInt()}")
            } else {
                setInputError(container, "")
                updateSharedProfile()
            }
        }

        input.setOnFocusChangeListener{it, hasFocus ->
            if (!hasFocus) {
                val text = (it as TextInputEditText).text.toString()

                if (text.isNullOrEmpty() || Integer.parseInt(text) < min || Integer.parseInt(
                                text
                        ) > max
                ) {
                    if (!init) setInputError(container, "${min.roundToInt()} - ${max.roundToInt()}")
                } else {
                    setInputError(container, "")
                }
            }
        }
    }


    private fun setInputError(container: TextInputLayout, msg: String) {
        container.error = msg
    }

    private fun navigateToMainForm() {
        requireActivity().supportFragmentManager.popBackStack()
    }

    private fun getSaveRecommendRequest():SaveRecommendRequest {
        val request = SaveRecommendRequest()

        request.sk = data?.sk
        request.ck = data?.ck
        request.sex = selectedSex
        request.user = user
        request.fit = sbFitLooseControl.progress
        request.w = RecommendationWay.ADV_WAY

        if(isMetric) {
            request.height = if (heightCm > 0) heightCm.toString() else tiEtHeightMetric.text.toString().toFloat().toString()
            request.chest = if (chestCm > 0) chestCm.toString() else tiEtChestMetric.text.toString().toFloat().toString()
            request.waist = if (waistCm > 0) waistCm.toString() else  tiEtWaistMetric.text.toString().toFloat().toString()
            request.hips = if (hipsCm > 0) hipsCm.toInt().toString() else tiEtHipsMetric.text.toString().toFloat().toString()
            request.metricSystem = MetricSystem.METRIC
            request.massSystem = MetricSystem.METRIC
        } else {
            request.height = if (heightTotalInches > 0) heightTotalInches.toString() else (tiEtHeightFeetImperial.text.toString().toFloat() * 12 + tiEtHeightInchesImperial.text.toString().toFloat()).toString()
            request.chest = if (chestInches > 0) chestInches.toString() else tiEtChestImperial.text.toString().toFloat().toString()
            request.waist = if (waistInches > 0) waistInches.toString() else tiEtWaistImperial.text.toString().toFloat().toString()
            request.hips = if (hipsInches > 0) hipsInches.toInt().toString()else tiEtHipsImperial.text.toString().toFloat().toString()
            request.metricSystem = MetricSystem.IMPERIAL
            request.massSystem = MetricSystem.IMPERIAL
        }

        return request
    }

    private fun navigateToLastStepForm() {
        val request = getSaveRecommendRequest()


        requireActivity().supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
            .replace(
                    R.id.fragment_container, LastStepFormFragment.newInstance(
                    product!!,
                    data!!,
                    request,
                    true,
                    selectedSex))
            .addToBackStack(null)
            .commit()
    }

    private fun updateSubjectData() {
        subjectDataMetric = DataExtractionUtils.getSubjectDataBySex(data?.data?.dataMetric!!, data?.sex!!, selectedSex)
        subjectDataImperial = DataExtractionUtils.getSubjectDataBySex(data?.data?.dataImperial!!, data?.sex!!, selectedSex)
    }

    private fun updateDataAndViewsOnSexChange() {
        updateSubjectData()
        initMeasuresValues()
        setDefaultFormValues()
        initSeekBar()
    }
}