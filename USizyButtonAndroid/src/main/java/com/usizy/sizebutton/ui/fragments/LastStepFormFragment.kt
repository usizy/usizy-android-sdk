package com.usizy.sizebutton.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.view.children
import androidx.core.view.get
import androidx.core.widget.doAfterTextChanged
import androidx.core.widget.doOnTextChanged
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.usizy.sizebutton.R
import com.usizy.sizebutton.ui.views.checkables.ImageSelectorButton
import com.usizy.sizebutton.ui.views.checkables.ImageSelectorRadioGroup
import com.usizy.sizebutton.ui.views.checkables.RadioImageSelectorButton
import com.usizy.sizebutton.utils.DataExtractionUtils
import com.usizy.sizequeryandroid.MeasurementsValues
import com.usizy.sizequeryandroid.SexValues
import com.usizy.sizequeryandroid.ZoneValues
import com.usizy.sizequeryandroid.model.*
import kotlin.math.roundToInt

private const val ARG_PRODUCT = "ARG_PRODUCT"
private const val ARG_DATA = "ARG_DATA"
private const val ARG_REQUEST = "ARG_REQUEST"
private const val ARG_IS_FROM_ADVANCED = "ARG_IS_FROM_ADVANCED"
private const val ARG_SELECTED_SEX = "ARG_SELECTED_SEX"
private val SEX_WITH_SELECTOR = arrayOf(SexValues.MALE, SexValues.FEMALE, SexValues.UNISEX)

/**
 * A simple [Fragment] subclass.
 * Use the [LastStepFormFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LastStepFormFragment : Fragment() {
    private var product: String? = null
    private var data: PopupData? = null
    private var request: SaveRecommendRequest? = null
    private var selectedSex: String? = null
    private var isFromAdvanced: Boolean = false
    private var showChestImageSelector: Boolean = false
    private var showWaistImageSelector: Boolean = false
    private var showHipsImageSelector: Boolean = false
    private var sex: String? = null
    private var chestType: Int? = null
    private var waistType: Int? = null
    private var hipsType: Int? = null

    private lateinit var btnRecommendMe: Button
    private lateinit var btnGoBack: Button
    private lateinit var llChestMale: LinearLayout
    private lateinit var llChestFemale: LinearLayout
    private lateinit var llWaistMale: LinearLayout
    private lateinit var llWaistFemale: LinearLayout
    private lateinit var llHipsFemale: LinearLayout

    private lateinit var rgChestMale: ImageSelectorRadioGroup
    private lateinit var rgChestFemale: ImageSelectorRadioGroup
    private lateinit var rgWaistMale: ImageSelectorRadioGroup
    private lateinit var rgWaistFemale: ImageSelectorRadioGroup
    private lateinit var rgHipsFemale: ImageSelectorRadioGroup

    private lateinit var tilAge: TextInputLayout
    private lateinit var tietAge: TextInputEditText

    private lateinit var ageLimitsMetric: MeasureLimitMetric
    private var subjectData: SubjectData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            product = it.getString(ARG_PRODUCT)
            data = Gson().fromJson(it.getString(ARG_DATA), PopupData::class.java)
            isFromAdvanced = it.getBoolean(ARG_IS_FROM_ADVANCED)
            request = Gson().fromJson(it.getString(ARG_REQUEST), SaveRecommendRequest::class.java)
            selectedSex = it.getString(ARG_SELECTED_SEX)
        }
        ageLimitsMetric = data?.sexLimits!!.metric.age
        subjectData = DataExtractionUtils.getSubjectDataBySex(data?.data?.dataMetric!!, data?.sex!!, selectedSex)

        val measurements = data?.measurements
        sex = if (!selectedSex.isNullOrEmpty()) selectedSex else data?.sex

        val zone = data?.zone

        if (SEX_WITH_SELECTOR.contains(sex) && !isFromAdvanced && (measurements!!.contains(MeasurementsValues.CHEST) || zone != ZoneValues.LOWER)) {
            showChestImageSelector = true
        }

        if (SEX_WITH_SELECTOR.contains(sex) && !isFromAdvanced) {
            showWaistImageSelector = true
        }

        if (sex == SexValues.FEMALE && !isFromAdvanced && (zone == ZoneValues.LOWER || measurements!!.contains(MeasurementsValues.HIPS))) {
            showHipsImageSelector = true
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_last_step_form, container, false)
        initViews(rootView)
        setOnClickListeners()
        setOnChangeTextListeners()
        hideImageSelectorsOnInit()
        loadImageSelectors()

        return rootView
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param product
         * @param data
         * @param isFromAdvanced checking if coming from advanced form (your measurements) or main
         * @param selectedSex sex string
         * @return A new instance of fragment LastStepFormFragment.
         */
        @JvmStatic
        fun newInstance(product: String, data: PopupData, request: SaveRecommendRequest, isFromAdvanced: Boolean, selectedSex: String?) =
            LastStepFormFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PRODUCT, product)
                    putString(ARG_DATA, Gson().toJson(data))
                    putString(ARG_REQUEST, Gson().toJson(request))
                    putBoolean(ARG_IS_FROM_ADVANCED, isFromAdvanced)
                    putString(ARG_SELECTED_SEX, selectedSex)
                }
            }
    }

    private fun initViews(it: View) {
        btnGoBack = it.findViewById(R.id.btn_go_back)
        btnRecommendMe = it.findViewById(R.id.btn_recommend_me)

        llChestMale = it.findViewById(R.id.ll_chest_male_selector)
        llChestFemale = it.findViewById(R.id.ll_chest_female_selector)
        llWaistMale = it.findViewById(R.id.ll_waist_male_selector)
        llWaistFemale = it.findViewById(R.id.ll_waist_female_selector)
        llHipsFemale = it.findViewById(R.id.ll_hips_female_selector)

        rgChestFemale = it.findViewById(R.id.rg_female_chest)
        rgChestMale = it.findViewById(R.id.rg_male_chest)
        rgHipsFemale = it.findViewById(R.id.rg_female_hips)
        rgWaistFemale = it.findViewById(R.id.rg_female_waist)
        rgWaistMale = it.findViewById(R.id.rg_male_waist)

        tilAge = it.findViewById(R.id.til_age)
        tietAge = it.findViewById(R.id.ti_et_age)
        tietAge.setSelectAllOnFocus(true)
        if (subjectData != null) {
            val age = subjectData!!.age.toString()
            tietAge.setText(age)
        }

    }

    private fun hideImageSelectorsOnInit() {
        llChestMale.visibility = View.GONE
        llChestFemale.visibility = View.GONE
        llWaistMale.visibility = View.GONE
        llWaistFemale.visibility = View.GONE
        llHipsFemale.visibility = View.GONE
    }

    private fun loadImageSelectors() {
        if (showChestImageSelector) {
            when (sex) {
                SexValues.MALE -> {
                   llChestMale.visibility = View.VISIBLE
                    setRadioGroupChildren(rgChestMale)
                }
                SexValues.FEMALE -> {
                    llChestFemale.visibility = View.VISIBLE
                    setRadioGroupChildren(rgChestFemale)
                }
            }
        }

        if (showWaistImageSelector) {
            when (sex) {
                SexValues.MALE -> {
                    llWaistMale.visibility = View.VISIBLE
                    setRadioGroupChildren(rgWaistMale)
                }
                SexValues.FEMALE -> {
                    llWaistFemale.visibility = View.VISIBLE
                    setRadioGroupChildren(rgWaistFemale)
                }
            }
        }

        if (showHipsImageSelector) {
            llHipsFemale.visibility = View.VISIBLE
            setRadioGroupChildren(rgHipsFemale)
        }
    }

    private fun setRadioGroupChildren(radioGroup: ImageSelectorRadioGroup) {
        radioGroup.children.forEach { view ->
            if (view is RadioImageSelectorButton) {
                when (radioGroup.getType()) {
                    MeasurementsValues.CHEST -> {
                        if (subjectData!!.chestType != 0) {
                            view.isChecked = view.getValue() == subjectData!!.chestType
                        } else {
                            view.isChecked = view.getValue() == 1
                        }
                    }
                    MeasurementsValues.WAIST -> {
                        if (subjectData!!.waistType != 0) {
                            view.isChecked = view.getValue() == subjectData!!.waistType
                        } else {
                            view.isChecked = view.getValue() == 1
                        }
                    }
                    MeasurementsValues.HIPS -> {
                        if (subjectData!!.hipsType != 0) {
                            view.isChecked = view.getValue() == subjectData!!.hipsType
                        } else {
                            view.isChecked = view.getValue() == 1
                        }
                    }
                }
                view.setOnClickListener {
                    val button: ImageSelectorButton = it as ImageSelectorButton
                    val type = radioGroup.getType()
                    val selectedValue = button.getValue()
                    when (type) {
                        MeasurementsValues.CHEST -> chestType = selectedValue
                        MeasurementsValues.WAIST -> waistType = selectedValue
                        MeasurementsValues.HIPS -> hipsType = selectedValue
                        else -> {
                            chestType = null
                            waistType = null
                            hipsType = null
                        }
                    }
                }
            }
        }
    }

    private fun setOnClickListeners() {
        btnGoBack.setOnClickListener {
            navigateToBack()
        }

        btnRecommendMe.setOnClickListener {
            if(tilAge.error.isNullOrBlank()) {
                navigateToRecommend()
            }
        }
    }

    private fun setOnChangeTextListeners() {
        tietAge.doOnTextChanged { text, _, _, _ ->
            if (text.isNullOrEmpty() || Integer.parseInt(text.toString()) < ageLimitsMetric.min || Integer.parseInt(
                    text.toString()
                ) > ageLimitsMetric.max
            ) {
                tilAge.error = "${ageLimitsMetric.min.roundToInt()} - ${ageLimitsMetric.max.roundToInt()}"
            } else {
                tilAge.error = ""
            }
        }

        tietAge.doAfterTextChanged {
            val text = it.toString()
            if (text.isNullOrEmpty() || Integer.parseInt(text) < ageLimitsMetric.min || Integer.parseInt(
                            text
                    ) > ageLimitsMetric.max
            ) {
                tilAge.error = "${ageLimitsMetric.min.roundToInt()} - ${ageLimitsMetric.max.roundToInt()}"
            } else {
                tilAge.error = ""
            } }
    }

    private fun navigateToBack() {
        requireActivity().supportFragmentManager.popBackStack()
    }

    private fun getSaveRecommendRequest():SaveRecommendRequest?{
        request?.age = tietAge.text.toString().toInt()

        request?.hipsType = if (hipsType != null) hipsType else 0
        request?.waistType = if (waistType != null) waistType else 0
        request?.chestType = if (chestType != null) chestType else 0

        return request
    }

    private fun navigateToRecommend() {
        if (product == null || request == null || data == null) return

        val request = getSaveRecommendRequest()
        val fragment = ResultsFragment.newInstance(
                product!!,
                request!!,
                data!!
        )
        requireActivity().supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit()
    }
}