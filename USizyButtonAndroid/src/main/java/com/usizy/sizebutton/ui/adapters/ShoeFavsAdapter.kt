package com.usizy.sizebutton.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.usizy.sizebutton.R
import com.usizy.sizequeryandroid.model.ShoeFavs

class ShoeFavsAdapter(private val list: List<ShoeFavs>) : BaseAdapter() {

    var shoeList = list

    override fun getView(position: Int, convertView: View?, container: ViewGroup): View? {

        val inflater = container.context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.shoe_fav_row_item, null)

        val tvShoeFavText = view.findViewById<TextView>(R.id.tv_shoe_fav_text)
        val ivShoeFavThumb = view.findViewById<ImageView>(R.id.iv_shoe_fav_thumb)

        tvShoeFavText.text = shoeList[position].text
        Glide.with(container.context).load(shoeList[position].thumb).into(ivShoeFavThumb)

        return view
    }

    override fun getItem(position: Int): Any {
        return shoeList[position]
    }

    override fun getItemId(position: Int): Long {
        return shoeList[position].id.toLong()
    }

    override fun getCount(): Int {
        return shoeList.size
    }

    public fun update(data: List<ShoeFavs>) {
        shoeList = data
        notifyDataSetChanged()
    }
}
