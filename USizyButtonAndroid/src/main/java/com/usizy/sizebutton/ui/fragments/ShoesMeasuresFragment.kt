package com.usizy.sizebutton.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.usizy.sizequeryandroid.model.PopupData
import com.usizy.sizebutton.R
import com.usizy.sizebutton.utils.DataExtractionUtils
import com.usizy.sizequeryandroid.MetricSystem
import com.usizy.sizequeryandroid.RecommendationWay
import com.usizy.sizequeryandroid.SexValues
import com.usizy.sizequeryandroid.model.SaveRecommendRequest
import com.usizy.sizequeryandroid.model.SubjectData
import com.usizy.sizequeryandroid.model.SubjectDataImperial
import kotlin.math.round


private const val ARG_DATA = "ARG_DATA"
private const val ARG_PRODUCT = "ARG_PRODUCT"
private const val ARG_USER = "ARG_USER"

/**
 * A simple [Fragment] subclass.
 * Use the [ShoesMeasuresFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ShoesMeasuresFragment : Fragment() {
    private var dataMetric: SubjectData? = null
    private var dataImperial: SubjectDataImperial? = null
    private var product: String? = null
    private var data: PopupData? = null
    private var user: String? = null

    private var isMetric: Boolean = true
    private var footLimitSelectedMetric: Float = 0.0f
    private var footLimitSelectedImperial: Float = 0.0f

    private lateinit var iv_product_main: ImageView
    private lateinit var tgBtnCmIn: ToggleButton
    private lateinit var spFoot: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            product = it.getString(ARG_PRODUCT)
            data = Gson().fromJson(it.getString(ARG_DATA), PopupData::class.java)
            user = it.getString(ARG_USER)
        }
        dataMetric = DataExtractionUtils.getSubjectDataBySex(data?.data?.dataMetric!!, data?.sex!!, null)
        dataImperial = DataExtractionUtils.getSubjectDataBySex(data?.data?.dataImperial!!, data?.sex!!, null)
        footLimitSelectedMetric = dataMetric?.foot!!
        footLimitSelectedImperial = dataImperial?.foot!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_shoes_measures, container, false)
        initViews(rootView)
        return rootView
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param product productId.
         * @param user userId.
         * @param data PopupData.
         * @return A new instance of fragment ShoesFormFragment.
         */
        @JvmStatic
        fun newInstance(product: String, user: String, data: PopupData) =
                ShoesMeasuresFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_DATA, Gson().toJson(data))
                        putString(ARG_PRODUCT, product)
                        putString(ARG_USER, user)
                    }
                }
    }

    private fun initViews(it: View) {
        initImageProduct(it)
        initToggleCnIn(it)
        initFootSpinner(it)
        initMeasuresButton(it)
        initBtContinue(it)
    }

    private fun initImageProduct(it: View){
        iv_product_main = it.findViewById(R.id.iv_product_main)
        if (!data?.image.isNullOrBlank()) {
            Glide.with(this).load(data?.image).into(iv_product_main)
            iv_product_main.visibility = View.VISIBLE
        }
    }

    private fun initToggleCnIn(it: View){
        tgBtnCmIn = it.findViewById(R.id.toggle_cm_in)

        tgBtnCmIn.setOnCheckedChangeListener { _, checked ->
            isMetric = !checked
            updateFootSpinnerAdapter()
        }
    }

    private fun initMeasuresButton(it: View){
        (it.findViewById<View>(R.id.tv_to_basic_measures))
                .setOnClickListener(View.OnClickListener {
                    navigateToShoesForm()
                })
    }

    private fun initFootSpinner(it: View){
        spFoot = it.findViewById(R.id.spFoot)
        updateFootSpinnerAdapter()
        spFoot.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>?, view: View?, position: Int, id: Long){
                val config =  if (isMetric) data?.sexLimits?.metric?.foot else data?.sexLimits?.imperial?.foot
                if (config != null) {
                    val min = config.min
                    val step = config.step

                    if (isMetric) footLimitSelectedMetric = min + (position * step)
                    if (!isMetric) footLimitSelectedImperial =  min + (position * step)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>){ }
        }
    }

    private fun initBtContinue(it: View){
        it.findViewById<Button>(R.id.btn_main_continue).setOnClickListener {
            navigateToShoesImageSelectorForm()
        }
    }

    private fun updateFootSpinnerAdapter(){

        val config =  if (isMetric) data?.sexLimits?.metric?.foot else data?.sexLimits?.imperial?.foot
        val suffix = if (isMetric) "cm" else "in"

        if (config != null){

            spFoot.visibility = View.VISIBLE

            val options = ArrayList<String>()

            val max = config.max
            val min = config.min
            val step = config.step

            var i = min
            while (i < max) {
                options.add("$i $suffix")
                i = round((i + step) * 100) / 100 //Fix float round
            }

            val adapter = activity?.let { it1 ->
                ArrayAdapter(
                        it1,
                        android.R.layout.simple_spinner_item,
                        options
                )
            }
            adapter?.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
            spFoot.adapter = adapter

            val selPosition = if (isMetric) adapter?.getPosition("${footLimitSelectedMetric.toString()} $suffix") else adapter?.getPosition("${footLimitSelectedImperial.toString()} $suffix")
            if (selPosition != -1) {
                spFoot.setSelection(selPosition ?: 0)
            }

        }else{
            spFoot.visibility = View.GONE
        }
    }

    private fun navigateToShoesForm() {
        requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, ShoesFormFragment.newInstance(
                        product!!,
                        user!!,
                        data!!
                ))
                .addToBackStack(null)
                .commit()
    }

    private fun navigateToShoesImageSelectorForm() {
        val request = getSaveRecommendRequest()
        requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, ShoesImageSelectorFragment.newInstance(
                        product!!,
                        user!!,
                        data!!,
                        request
                ))
                .addToBackStack(null)
                .commit()
    }

    private fun getSaveRecommendRequest():SaveRecommendRequest{
        val request = SaveRecommendRequest()

        request.sk = data?.sk
        request.ck = data?.ck
        request.sex = if (data?.sex === SexValues.FEMALE) data?.sex else SexValues.MALE
        request.user = user
        request.w = RecommendationWay.ADV_WAY
        request.foot = if (isMetric) footLimitSelectedMetric else footLimitSelectedImperial

        if (isMetric){
            request.age = if (data?.sex === SexValues.FEMALE) data?.data?.dataMetric?.female?.age else data?.data?.dataMetric?.male?.age
            request.footType = if (data?.sex === SexValues.FEMALE) data?.data?.dataMetric?.female?.footType else data?.data?.dataMetric?.male?.footType
            request.footWidth = if (data?.sex === SexValues.FEMALE) data?.data?.dataMetric?.female?.footWidth else data?.data?.dataMetric?.male?.footWidth
            request.metricSystem = MetricSystem.METRIC
        }else{
            request.age = if (data?.sex === SexValues.FEMALE) data?.data?.dataImperial?.female?.age else data?.data?.dataImperial?.male?.age
            request.footType = if (data?.sex === SexValues.FEMALE) data?.data?.dataImperial?.female?.footType else data?.data?.dataImperial?.male?.footType
            request.footWidth = if (data?.sex === SexValues.FEMALE) data?.data?.dataImperial?.female?.footWidth else data?.data?.dataImperial?.male?.footWidth
            request.metricSystem = MetricSystem.IMPERIAL
        }
        return request
    }
}