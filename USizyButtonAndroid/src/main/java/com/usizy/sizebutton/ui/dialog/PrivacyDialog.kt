package com.usizy.sizebutton.ui.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.Window
import com.usizy.sizebutton.R


class PrivacyDialog{
    fun show(activity: Activity) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_privacy)

        (dialog.findViewById(R.id.ivPrivacyLogo) as View).setOnClickListener(View.OnClickListener {
            val openURL = Intent(android.content.Intent.ACTION_VIEW)
            openURL.data = Uri.parse(activity.getString((R.string.privacy_link)))
            activity.startActivity(openURL)
        })

        (dialog.findViewById(R.id.ivPrivacyVisit) as View).setOnClickListener(View.OnClickListener {
            val openURL = Intent(android.content.Intent.ACTION_VIEW)
            openURL.data = Uri.parse(activity.getString((R.string.privacy_link)))
            activity.startActivity(openURL)
        })

        (dialog.findViewById(R.id.privacyTerms) as View).setOnClickListener(View.OnClickListener {
            val openURL = Intent(android.content.Intent.ACTION_VIEW)
            openURL.data = Uri.parse(activity.getString((R.string.privacy_href2)))
            activity.startActivity(openURL)
        })

        (dialog.findViewById(R.id.privacyPrivacy) as View).setOnClickListener(View.OnClickListener {
            val openURL = Intent(android.content.Intent.ACTION_VIEW)
            openURL.data = Uri.parse(activity.getString((R.string.privacy_href1)))
            activity.startActivity(openURL)
        })
        dialog.show()
    }
}