package com.usizy.sizebutton.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.view.children
import com.google.gson.Gson
import com.usizy.sizebutton.R
import com.usizy.sizebutton.ui.views.checkables.ImageSelectorButton
import com.usizy.sizebutton.ui.views.checkables.ImageSelectorRadioGroup
import com.usizy.sizebutton.ui.views.checkables.RadioImageSelectorButton
import com.usizy.sizebutton.utils.DataExtractionUtils
import com.usizy.sizequeryandroid.MeasurementsValues
import com.usizy.sizequeryandroid.model.PopupData
import com.usizy.sizequeryandroid.model.SaveRecommendRequest
import com.usizy.sizequeryandroid.model.SubjectData

private const val ARG_DATA = "ARG_DATA"
private const val ARG_PRODUCT = "ARG_PRODUCT"
private const val ARG_USER = "ARG_USER"
private const val ARG_REQUEST = "ARG_REQUEST"

/**
 * A simple [Fragment] subclass.
 * Use the [ShoesImageSelectorFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ShoesImageSelectorFragment : Fragment() {
    private var product: String? = null
    private var data: PopupData? = null
    private var user: String? = null
    private var request: SaveRecommendRequest? = null
    private var subjectData: SubjectData? = null
    private var footType: Int? = null

    private lateinit var rgFeeType: ImageSelectorRadioGroup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            product = it.getString(ARG_PRODUCT)
            data = Gson().fromJson(it.getString(ARG_DATA), PopupData::class.java)
            user = it.getString(ARG_USER)
            request = Gson().fromJson(it.getString(ARG_REQUEST), SaveRecommendRequest::class.java)
        }

        subjectData = DataExtractionUtils.getSubjectDataBySex(data?.data?.dataMetric!!, data?.sex!!, null)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_shoes_image_selector, container, false)
        initViews(rootView)
        return rootView
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param product productId.
         * @param user userId.
         * @param data PopupData.
         * @param request SaveRecommendRequest.
         * @return A new instance of fragment ShoesImageSelectorFragment.
         */
        @JvmStatic
        fun newInstance(product: String, user: String, data: PopupData, request: SaveRecommendRequest) =
            ShoesImageSelectorFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_DATA, Gson().toJson(data))
                    putString(ARG_PRODUCT, product)
                    putString(ARG_USER, user)
                    putString(ARG_REQUEST, Gson().toJson(request))
                }
            }
    }

    private fun initViews(it: View) {
        setOnClickListeners(it)

        rgFeeType = it.findViewById(R.id.rg_feet_type)

        setRadioGroupChildren(rgFeeType)
    }

    private fun setRadioGroupChildren(radioGroup: ImageSelectorRadioGroup) {
        radioGroup.children.forEach { view ->
            if (view is RadioImageSelectorButton) {
                    if (subjectData!!.footType != 0) {
                        view.isChecked = view.getValue() == subjectData!!.footType
                    } else {
                        view.isChecked = view.getValue() == 1
                    }
                }
                view.setOnClickListener {
                    val button: ImageSelectorButton = it as ImageSelectorButton
                    footType = button.getValue()
                }
            }
        }


    private fun setOnClickListeners(it: View) {
        it.findViewById<Button>(R.id.btn_go_back).setOnClickListener {
            navigateToBack()
        }

        it.findViewById<Button>(R.id.btn_recommend_me).setOnClickListener {
            navigateToRecommend()
        }
    }

    private fun navigateToBack() {
        requireActivity().supportFragmentManager.popBackStack()
    }

    private fun navigateToRecommend() {
        if (product == null || request == null || data == null) return

        val request = getSaveRecommendRequest()
        val fragment = ResultsFragment.newInstance(
            product!!,
            request!!,
            data!!
        )
        requireActivity().supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun getSaveRecommendRequest():SaveRecommendRequest?{
        request?.footType = if (footType != null) footType else 0

        return request
    }

}