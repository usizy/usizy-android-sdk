package com.usizy.sizebutton.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.widget.doAfterTextChanged
import androidx.core.widget.doBeforeTextChanged
import androidx.core.widget.doOnTextChanged
import com.bumptech.glide.Glide
import com.usizy.sizebutton.R
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.usizy.sizebutton.constants.*
import com.usizy.sizebutton.model.Profile
import com.usizy.sizebutton.model.SharedSexProfile
import com.usizy.sizebutton.utils.DataExtractionUtils
import com.usizy.sizebutton.utils.FormValidationHelpers
import com.usizy.sizebutton.utils.InputShape
import com.usizy.sizebutton.utils.MetricsConvertHelpers
import com.usizy.sizequeryandroid.MetricSystem
import com.usizy.sizequeryandroid.RecommendationWay
import com.usizy.sizequeryandroid.SexValues
import com.usizy.sizequeryandroid.model.*
import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.roundToInt

private const val ARG_DATA = "ARG_DATA"
private const val ARG_PRODUCT = "ARG_PRODUCT"
private const val ARG_USER = "ARG_USER"

/**
 * A simple [Fragment] subclass.
 * Use the [MainFormFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MainFormFragment : Fragment() {
    private var product: String? = null
    private var data: PopupData? = null
    private var user: String? = null

    private lateinit var inputsList: MutableCollection<InputShape>
    private lateinit var limitsMetric: LimitsMetric
    private lateinit var limitsImperial: LimitsImperial
    private var isOnlyHeight: Boolean = false
    private var subjectDataMetric: SubjectData? = null
    private var subjectDataImperial: SubjectDataImperial? = null
    private var selectedSex: String? = null
    private var isMetric: Boolean = true
    private var imageProductURL: String? = null
    private var heightCm: Float = 0f
    private var heightFeet: Float = 0f
    private var heightInches: Float = 0f
    private var heightTotalInches: Float = 0f
    private var weightKg: Float = 0f
    private var weightLbs: Float = 0f
    private var age: Int = 0
    private var fitValue: Int = 0

    private var init = true

    private var minInchesHeight: Float = 0f
    private var maxInchesHeight: Float = 0f
    private var currentInchesHeight: Int = 0
    private var sexProfile: Profile? = null

    private lateinit var tvYourMeasures : TextView
    private lateinit var sbFitLooseControl: SeekBar
    private lateinit var btnFitter: Button
    private lateinit var btnLooser: Button
    private lateinit var btnContinue: Button
    private lateinit var tilHeightMetric: TextInputLayout
    private lateinit var tiEtHeightMetric: TextInputEditText
    private lateinit var tilWeightMetric: TextInputLayout
    private lateinit var tiEtWeightMetric: TextInputEditText
    private lateinit var tilFeetImperial: TextInputLayout
    private lateinit var tiEtFeetImperial: TextInputEditText
    private lateinit var tilInchesImperial: TextInputLayout
    private lateinit var tiEtInchesImperial: TextInputEditText
    private lateinit var tilWeightImperial: TextInputLayout
    private lateinit var tiEtWeightImperial: TextInputEditText
    private lateinit var tgBtnCmIn: ToggleButton
    private lateinit var formInputsMetricView: View
    private lateinit var formInputsImperialView: View
    private lateinit var llInputWeightMetric: LinearLayout
    private lateinit var llInputAgeMetric: LinearLayout
    private lateinit var llInputWeightImperial: LinearLayout
    private lateinit var llInputAgeImperial: LinearLayout
    private lateinit var tilAgeMainMetric: TextInputLayout
    private lateinit var tiEtAgeMainMetric: TextInputEditText
    private lateinit var tilAgeMainImperial: TextInputLayout
    private lateinit var tiEtAgeMainImperial: TextInputEditText
    private lateinit var ivProductImage: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            product = it.getString(ARG_PRODUCT)
            data = Gson().fromJson(it.getString(ARG_DATA), PopupData::class.java)
            user = it.getString(ARG_USER)
        }
        inputsList = mutableListOf()
        limitsImperial = data?.sexLimits!!.imperial
        limitsMetric = data?.sexLimits!!.metric
        updateSubjectData()
        isOnlyHeight = data?.onlyHeight!!

        minInchesHeight = limitsImperial.height.min.ft * 12 + limitsImperial.height.min.inX
        maxInchesHeight = limitsImperial.height.max.ft * 12 + limitsImperial.height.max.inX

        currentInchesHeight = (subjectDataImperial?.height!!.ft * 12 + subjectDataImperial?.height!!.inX).toInt()
        imageProductURL = data?.image

        initSelectedSex()
        initMeasuresValues()
    }

    private fun initMeasuresValues() {
        heightCm = sexProfile?.metricHeight ?: subjectDataMetric?.height!!
        heightFeet = sexProfile?.imperialHeight?.ft ?: subjectDataImperial?.height!!.ft
        heightInches = sexProfile?.imperialHeight?.inX ?: subjectDataImperial?.height!!.inX
        heightTotalInches = heightFeet * 12 + heightInches
        weightKg = sexProfile?.weightMet ?: subjectDataMetric?.weight!!
        weightLbs = sexProfile?.weightImp ?: subjectDataImperial?.weight!!
        age = sexProfile?.age ?: subjectDataMetric?.age!!
        fitValue = sexProfile?.fit ?: subjectDataMetric?.fit!!

        persistSharedData()
    }

    private fun initSelectedSex() {
        if (data?.sex == SexValues.UNISEX) {
            selectedSex = SharedSexProfile.selectedSex ?: SexValues.MALE
            SharedSexProfile.selectedSex = selectedSex
        } else if (data?.sex == SexValues.HUMAN) {
            selectedSex = SharedSexProfile.selectedSex ?: SexValues.BOY
            SharedSexProfile.selectedSex = selectedSex
        } else {
            selectedSex = null
            SharedSexProfile.selectedSex = null
        }
        sexProfile = SharedSexProfile.getProfile(data?.sex)
    }

    private fun updateSharedProfile(){
        if (isMetric){
            if (!tiEtAgeMainMetric.text.toString().isNullOrEmpty()){
                age = tiEtAgeMainMetric.text.toString().toInt()
            }
            if (!tiEtWeightMetric.text.toString().isNullOrEmpty()){
                weightKg = tiEtWeightMetric.text.toString().toFloat()
            }
            if (!tiEtHeightMetric.text.toString().isNullOrEmpty()){
                heightCm =  tiEtHeightMetric.text.toString().toFloat()
            }
        }else{
            if (!tiEtAgeMainImperial.text.toString().isNullOrEmpty()){
                age = tiEtAgeMainImperial.text.toString().toInt()
            }
            if (!tiEtWeightImperial.text.toString().isNullOrEmpty()){
                weightLbs =  tiEtWeightImperial.text.toString().toFloat()
            }
            if (!tiEtFeetImperial.text.toString().isNullOrEmpty()){
                heightFeet = tiEtFeetImperial.text.toString().toFloat()
            }
            if (!tiEtInchesImperial.text.toString().isNullOrEmpty()){
                heightInches = tiEtInchesImperial.text.toString().toFloat()
            }
        }

        persistSharedData()
    }

    private fun persistSharedData() {
        sexProfile?.metricHeight = heightCm
        sexProfile?.imperialHeight = HeightImperial(heightFeet, heightInches)
        sexProfile?.weightMet = weightKg
        sexProfile?.weightImp = weightLbs
        sexProfile?.fit = fitValue
        sexProfile?.age = age
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_main_form, container, false)
        initViews(rootView)
        setOnClickListeners()
        initSeekBar()
        handleInputsLoad()
        initInputsListeners()
        setImperialHeightInputsListeners()
        setDefaultFormValues()

        init = false
        return rootView
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param product productId.
         * @param data PopupData.
         * @return A new instance of fragment MainFormFragment.
         */
        @JvmStatic
        fun newInstance(product: String, user: String, data: PopupData) =
            MainFormFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_DATA, Gson().toJson(data) )
                    putString(ARG_PRODUCT, product)
                    putString(ARG_USER, user)
                }
            }
    }

    private fun initViews(it: View) {
        formInputsMetricView = it.findViewById(R.id.form_metric_inputs)
        formInputsImperialView = it.findViewById(R.id.form_imperial_inputs)
        llInputAgeImperial = it.findViewById(R.id.ll_input_age_imperial)
        llInputAgeMetric = it.findViewById(R.id.ll_input_age_metric)
        llInputWeightImperial = it.findViewById(R.id.ll_input_weight_imperial)
        llInputWeightMetric = it.findViewById(R.id.ll_input_weight_metric)

        tvYourMeasures = it.findViewById(R.id.tv_your_measures)
        sbFitLooseControl = it.findViewById(R.id.sb_fit_loose_control)
        btnFitter = it.findViewById(R.id.btn_fit_controller)
        btnLooser = it.findViewById(R.id.btn_loose_controller)
        tgBtnCmIn = it.findViewById(R.id.toggle_cm_in)
        btnContinue = it.findViewById(R.id.btn_main_continue)
        ivProductImage = it.findViewById(R.id.iv_product_main)


        initMetricInputs(it)
        initImperialInputs(it)
        initSexRadioGroup(it)
        initInputsVisibility()
        initImageView()
    }

    private fun initImageView() {
        if (!imageProductURL.isNullOrBlank()) {
            Glide.with(this).load(imageProductURL).into(ivProductImage)
            ivProductImage.visibility = View.VISIBLE
        }
    }

    private fun initInputsVisibility() {
        if (isOnlyHeight) {
            llInputWeightMetric.visibility = View.GONE
            llInputWeightImperial.visibility = View.GONE
            llInputAgeMetric.visibility = View.VISIBLE
            llInputAgeImperial.visibility = View.VISIBLE
            tvYourMeasures.visibility = View.GONE
            btnContinue.setText(R.string.buttons_recommendme)
        }
    }

    private fun initMetricInputs(it: View) {
        tilHeightMetric = it.findViewById(R.id.til_height_metric)
        tiEtHeightMetric = it.findViewById(R.id.ti_et_height_metric)
        tiEtHeightMetric.setSelectAllOnFocus(true)

        tilWeightMetric = it.findViewById(R.id.til_weight_metric)
        tiEtWeightMetric = it.findViewById(R.id.ti_et_weight_metric)
        tiEtWeightMetric.setSelectAllOnFocus(true)

        tilAgeMainMetric = it.findViewById(R.id.til_age_main_metric)
        tiEtAgeMainMetric = it.findViewById(R.id.ti_et_age_main_metric)
        tiEtAgeMainMetric.setSelectAllOnFocus(true)
    }

    private fun initImperialInputs(it: View) {
        tilInchesImperial = it.findViewById(R.id.til_inches_main)
        tiEtInchesImperial = it.findViewById(R.id.ti_et_inches_main)
        tiEtInchesImperial.setSelectAllOnFocus(true)

        tilFeetImperial = it.findViewById(R.id.til_feet_main)
        tiEtFeetImperial = it.findViewById(R.id.ti_et_feet_main)
        tiEtFeetImperial.setSelectAllOnFocus(true)

        tilWeightImperial = it.findViewById(R.id.til_weight_imperial)
        tiEtWeightImperial = it.findViewById(R.id.ti_et_weight_imperial)
        tiEtWeightImperial.setSelectAllOnFocus(true)

        tilAgeMainImperial = it.findViewById(R.id.til_age_main_imperial)
        tiEtAgeMainImperial = it.findViewById(R.id.ti_et_age_main_imperial)
        tiEtAgeMainImperial.setSelectAllOnFocus(true)
    }

    private fun initSexRadioGroup(it: View) {
        if (data?.sex == SexValues.UNISEX){
            val iSex = it.findViewById<View>(R.id.iSex)
            iSex.visibility = View.VISIBLE

            val rgSex = it.findViewById<RadioGroup>(R.id.rgSex)
            rgSex.check(R.id.male)

            selectedSex = SexValues.MALE
            SharedSexProfile.selectedSex = selectedSex

            rgSex.setOnCheckedChangeListener { _, checkedId ->
                selectedSex = if (checkedId == R.id.male) SexValues.MALE else SexValues.FEMALE
                SharedSexProfile.selectedSex = selectedSex
                sexProfile = SharedSexProfile.getProfile(data?.sex)
                updateDataAndViewsOnSexChange()
            }
        } else if (data?.sex == SexValues.HUMAN) {
            val iHuman = it.findViewById<View>(R.id.iHuman)
            iHuman.visibility = View.VISIBLE

            val rgHuman = it.findViewById<RadioGroup>(R.id.rgHuman)
            rgHuman.check(R.id.adult)

            selectedSex = SexValues.MALE
            SharedSexProfile.selectedSex = selectedSex

            rgHuman.setOnCheckedChangeListener { _, checkedId ->
                selectedSex = if (checkedId == R.id.adult) SexValues.MALE else SexValues.BOY
                SharedSexProfile.selectedSex = selectedSex
                sexProfile = SharedSexProfile.getProfile(data?.sex)
                updateDataAndViewsOnSexChange()
            }
        }else{
            selectedSex = data?.sex
            SharedSexProfile.selectedSex = data?.sex
        }
        sexProfile = SharedSexProfile.getProfile(data?.sex)
    }

    private fun initSeekBar() {
        sbFitLooseControl.progress = subjectDataMetric!!.fit - 1
        sbFitLooseControl.max = SEEK_BAR_MAX_VALUE
        sbFitLooseControl.incrementProgressBy(SEEK_BAR_STEP_VALUE)
        setOnChangeSeekBarListener()
    }

    private fun setOnClickListeners() {
        /**
         * Navigates to your measures fragment
         * */
        tvYourMeasures.setOnClickListener {
            navigateToYourMeasuresForm()
        }

        /**
         * Changes -> Decrease -> the value of the fit property in the seekbar
         * */
        btnFitter.setOnClickListener {
            if(sbFitLooseControl.progress < SEEK_BAR_MIN_VALUE) {
                sbFitLooseControl.progress = SEEK_BAR_MIN_VALUE
                return@setOnClickListener
            }
            sbFitLooseControl.progress = sbFitLooseControl.progress - 1
        }

        /**
         * Changes -> Increase -> the value of the fit property in the seekbar
         * */
        btnLooser.setOnClickListener {
            if(sbFitLooseControl.progress > SEEK_BAR_MAX_VALUE) {
                sbFitLooseControl.progress = SEEK_BAR_MAX_VALUE
                return@setOnClickListener
            }
            sbFitLooseControl.progress = sbFitLooseControl.progress + 1
        }

        tgBtnCmIn.setOnCheckedChangeListener { _, checked ->
            if (!checked) {
                isMetric = true
                updateInputsAndValuesOnMetricChange()
                formInputsMetricView.visibility = View.VISIBLE
                formInputsImperialView.visibility = View.GONE
            } else {
                isMetric = false
                updateInputsAndValuesOnMetricChange()
                formInputsMetricView.visibility = View.GONE
                formInputsImperialView.visibility = View.VISIBLE
            }
            handleInputsLoad()
        }

        btnContinue.setOnClickListener {
            val filteredInputsByType: List<InputShape>
            val isValidForm: Boolean
            if (isMetric) {
                filteredInputsByType = inputsList.filter { it.type == METRIC_VALUE }
                isValidForm = FormValidationHelpers.isArrayInputsValid(inputsList = filteredInputsByType, checkForVisibleInputs = true)
            } else {
                filteredInputsByType = inputsList.filter { it.type == IMPERIAL_VALUE && !it.isImperialHeight}
                isValidForm = FormValidationHelpers.isArrayInputsValid(inputsList = filteredInputsByType, checkForVisibleInputs = true) && isImperialHeightValid()
            }

            if (isValidForm) {
                persistSharedData()
                if (!isOnlyHeight) {
                    navigateToLastStepForm()
                } else {
                    navigateToRecommend()
                }
            }
        }
    }


    private fun updateInputsAndValuesOnMetricChange() {
        if (isMetric) {
            updateHeightInputsAndValues(type = METRIC_VALUE)
            updateInputsAndValues(type = METRIC_VALUE)
        } else {
            updateHeightInputsAndValues(type = IMPERIAL_VALUE)
            updateInputsAndValues(type = IMPERIAL_VALUE)
        }
    }

    private fun updateHeightInputsAndValues(type: String) {
        when (type) {
            METRIC_VALUE -> {
                heightTotalInches = MetricsConvertHelpers.getTotalInchesFromImperial(HeightImperial(heightFeet, heightInches))
                heightCm = MetricsConvertHelpers.fromFeetInchesToCm(heightFeet, heightInches)
                tiEtHeightMetric.setText(floor(heightCm).toInt().toString())
            }
            IMPERIAL_VALUE -> {
                val inputValue: String? = tiEtHeightMetric.text.toString()
                if (!inputValue.isNullOrBlank()) {
                    heightCm = inputValue.toFloat()
                }
                heightTotalInches = MetricsConvertHelpers.getTotalInchesFromCm(heightCm)
                heightFeet = MetricsConvertHelpers.getImperialFromTotalInches(heightTotalInches).ft
                heightInches = MetricsConvertHelpers.getImperialFromTotalInches(heightTotalInches).inX
                tiEtFeetImperial.setText(floor(heightFeet).toInt().toString())
                tiEtInchesImperial.setText(floor(heightInches).toInt().toString())
            }
        }
    }

    private fun updateInputsAndValues(type: String) {
        when (type) {
            METRIC_VALUE -> {
                val inputValue: String? = tiEtWeightImperial.text.toString()
                if (!inputValue.isNullOrBlank()) {
                    val decPart = MetricsConvertHelpers.getDecimalPart(weightLbs)
                    weightLbs = inputValue.toFloat() + decPart
                }
                weightKg = MetricsConvertHelpers.fromPoundsToKgs(weightLbs)
                tiEtWeightMetric.setText(floor(weightKg).toInt().toString())
            }
            IMPERIAL_VALUE -> {
                val inputValue: String? = tiEtWeightMetric.text.toString()
                if (!inputValue.isNullOrBlank()) {
                    val decPart = MetricsConvertHelpers.getDecimalPart(weightKg)
                    weightKg = inputValue.toFloat() + abs(decPart)
                }
                weightLbs = MetricsConvertHelpers.fromKgToPounds(weightKg)
                tiEtWeightImperial.setText(floor(weightLbs).toInt().toString())
            }
        }
    }

    private fun isImperialHeightValid(): Boolean {
        val feet = tiEtFeetImperial.text.toString()
        val inches = tiEtInchesImperial.text.toString()
        val errorInFeet = manageHeightFeetErrorMessage(feet)
        val errorInInches = manageHeightInchesErrorMessage(inches)
        return !(errorInFeet || errorInInches)
    }

    private fun setOnChangeSeekBarListener() {
        /**
         * SeekBar for fitter control on change listeners
         * */
        sbFitLooseControl.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seek: SeekBar, progress: Int, fromUser: Boolean) {
                btnFitter.isEnabled = progress > SEEK_BAR_MIN_VALUE
                btnLooser.isEnabled = progress < SEEK_BAR_MAX_VALUE
                sexProfile?.fit = progress
            }
            override fun onStartTrackingTouch(seek: SeekBar) {
                // write custom code for progress is started
            }

            override fun onStopTrackingTouch(seek: SeekBar) {
            }
        })
    }

    private fun initInputsListeners() {
        for (input in inputsList) {
            if (!input.skipListener)
                setOnChangeTextListeners(input.container!!, input.input!!, input.max, input.min)
        }
    }

    private fun handleInputsLoad() {
        inputsList.add(InputShape(
                container = tilHeightMetric, input = tiEtHeightMetric,
                max = limitsMetric.height.max , min = limitsMetric.height.min,
                type = METRIC_VALUE, isInputFormHolderVisible = isMetric
        ))
        inputsList.add(InputShape(
                container = tilFeetImperial, input = tiEtFeetImperial,
                max = limitsImperial.height.max.ft, min = limitsImperial.height.min.ft,
                type = IMPERIAL_VALUE, isInputFormHolderVisible = !isMetric,
                skipListener = true, isImperialHeight = true
        ))
        inputsList.add(InputShape(
                container = tilInchesImperial, input = tiEtInchesImperial,
                max = limitsImperial.height.max.inX, min = limitsImperial.height.min.inX,
                type = IMPERIAL_VALUE, isInputFormHolderVisible = !isMetric,
                skipListener = true, isImperialHeight = true
        ))
        inputsList.add(InputShape(
                container = tilWeightMetric, input = tiEtWeightMetric,
                max = limitsMetric.weight.max , min = limitsMetric.weight.min,
                type = METRIC_VALUE, isInputFormHolderVisible = isMetric,
                inputVisible = llInputWeightMetric.visibility
        ))
        inputsList.add(InputShape(
                container = tilWeightImperial, input = tiEtWeightImperial,
                max = limitsImperial.weight.max , min = limitsImperial.weight.min,
                type = IMPERIAL_VALUE, isInputFormHolderVisible = !isMetric,
                inputVisible = llInputWeightImperial.visibility
        ))
        inputsList.add(InputShape(
                container = tilAgeMainMetric, input = tiEtAgeMainMetric,
                max = limitsMetric.age.max , min = limitsMetric.age.min,
                type = METRIC_VALUE, isInputFormHolderVisible = isMetric,
                inputVisible = llInputAgeMetric.visibility
        ))
        inputsList.add(InputShape(
                container = tilAgeMainImperial, input = tiEtAgeMainImperial,
                max = limitsImperial.age.max , min = limitsImperial.age.min,
                type = IMPERIAL_VALUE, isInputFormHolderVisible = !isMetric,
                inputVisible = llInputAgeImperial.visibility
        ))
    }

    private fun setDefaultFormValues() {
        tiEtHeightMetric.setText(heightCm.roundToInt().toString())
        tiEtFeetImperial.setText(floor(heightFeet).toInt().toString())
        tiEtInchesImperial.setText(floor(heightInches).toInt().toString())
        tiEtWeightMetric.setText(floor(weightKg).toInt().toString())
        tiEtWeightImperial.setText(floor(weightLbs).toInt().toString())
        tiEtAgeMainMetric.setText(subjectDataMetric!!.age.toString())
        tiEtAgeMainImperial.setText(subjectDataImperial!!.age.toString())
    }

    private fun setImperialHeightInputsListeners() {
        // FEET
        var intPartFt: Int
        var decPartFt = 0f
        tiEtFeetImperial.doOnTextChanged { text, _, _, _ ->
            manageHeightFeetErrorMessage(text = text.toString())
        }

        tiEtFeetImperial.doBeforeTextChanged { _, _, _, _ ->
            intPartFt = heightFeet.toInt()
            decPartFt = heightFeet - intPartFt
        }

        tiEtFeetImperial.doAfterTextChanged {
            manageHeightFeetErrorMessage(text = it.toString())
            if (!it.toString().isNullOrBlank()) {
                intPartFt = it.toString().toInt()
                heightFeet = intPartFt + decPartFt
            }
        }

        // INCHES
        var intPartIn: Int
        var decPartIn = 0f

        tiEtInchesImperial.doOnTextChanged { text, _, _, _ ->
            manageHeightInchesErrorMessage(text = text.toString())
        }

        tiEtInchesImperial.doBeforeTextChanged { _, _, _, _ ->
            intPartIn = heightInches.toInt()
            decPartIn = heightInches - intPartIn
        }

        tiEtInchesImperial.doAfterTextChanged {
            manageHeightInchesErrorMessage(text = it.toString())
            if (!it.toString().isNullOrBlank()) {
                intPartIn = it.toString().toInt()
                heightInches = intPartIn + decPartIn
            }
        }

        tiEtFeetImperial.setOnFocusChangeListener{it, hasFocus ->
            if (!hasFocus) {
                val text = (it as TextInputEditText).text.toString()
                manageHeightFeetErrorMessage(text)
            }
        }

        tiEtInchesImperial.setOnFocusChangeListener{it, hasFocus ->
            if (!hasFocus) {
                val text = (it as TextInputEditText).text.toString()
                manageHeightInchesErrorMessage(text)
            }
        }
    }

    private fun manageHeightInchesErrorMessage(text: String): Boolean {
        setCurrentInches(tiEtFeetImperial.text.toString(), text)
        var hasError = false
        if (text.isNullOrEmpty() || currentInchesHeight < minInchesHeight || currentInchesHeight > maxInchesHeight || Integer.parseInt(
                        text
                ) > 11) {
            if (!init) {
                setInputError(
                        tilInchesImperial,
                        "${limitsImperial.height.min.ft.roundToInt()} ft. ${limitsImperial.height.min.inX.roundToInt()} in. - ${limitsImperial.height.max.ft.roundToInt()} ft. ${limitsImperial.height.max.inX.roundToInt()} in."
                )
                setInputError(
                        tilFeetImperial,
                        "${limitsImperial.height.min.ft.roundToInt()} ft. ${limitsImperial.height.min.inX.roundToInt()} in. - ${limitsImperial.height.max.ft.roundToInt()} ft. ${limitsImperial.height.max.inX.roundToInt()} in."
                )
                hasError = true
            }
        } else {
            setInputError(tilInchesImperial, "")
            setInputError(tilFeetImperial, "")
        }
        return hasError
    }

    private fun manageHeightFeetErrorMessage(text: String): Boolean {
        setCurrentInches(text, tiEtInchesImperial.text.toString())
        var hasError = false
        if (text.isNullOrEmpty() || currentInchesHeight < minInchesHeight || currentInchesHeight > maxInchesHeight) {
            if(!init) {
                setInputError(
                        tilFeetImperial,
                        "${limitsImperial.height.min.ft.roundToInt()} ft. ${limitsImperial.height.min.inX.roundToInt()} in. - ${limitsImperial.height.max.ft.roundToInt()} ft. ${limitsImperial.height.max.inX.roundToInt()} in."
                )
                setInputError(
                        tilInchesImperial,
                        "${limitsImperial.height.min.ft.roundToInt()} ft. ${limitsImperial.height.min.inX.roundToInt()} in. - ${limitsImperial.height.max.ft.roundToInt()} ft. ${limitsImperial.height.max.inX.roundToInt()} in."
                )
                hasError = true
            }
        } else {
            setInputError(tilFeetImperial, "")
            setInputError(tilInchesImperial, "")
        }
        return hasError
    }

    private fun setCurrentInches(feet: String, inches: String) {
        if (!feet.isNullOrEmpty() && !inches.isNullOrEmpty()) {
            currentInchesHeight = Integer.parseInt(feet) * 12 + Integer.parseInt(inches)
        }
    }

    private fun setOnChangeTextListeners(container: TextInputLayout, input: TextInputEditText, max: Float, min: Float) {
        input.doOnTextChanged { text, _, _, _ ->
            if (text.isNullOrEmpty() || Integer.parseInt(text.toString()) < min || Integer.parseInt(
                    text.toString()
                ) > max
            ) {
                if (!init) setInputError(container,"${min.roundToInt()} - ${max.roundToInt()}")
            } else {
                setInputError(container,"")
                updateSharedProfile()
            }
        }

        input.doAfterTextChanged {
            val text = it.toString()
            if (text.isNullOrEmpty() || Integer.parseInt(text) < min || Integer.parseInt(
                            text
                    ) > max) {
                if (!init) setInputError(container,"${min.roundToInt()} - ${max.roundToInt()}")
            } else {
                setInputError(container,"")
            }

        }

        input.setOnFocusChangeListener{it, hasFocus ->
            if (!hasFocus) {
                val text = (it as TextInputEditText).text.toString()

                if (text.isNullOrEmpty() || Integer.parseInt(text) < min || Integer.parseInt(
                                text
                        ) > max
                ) {
                    if (!init) setInputError(container, "${min.roundToInt()} - ${max.roundToInt()}")
                } else {
                    setInputError(container, "")
                }
            }
        }
    }

    private fun setInputError(container: TextInputLayout, msg: String) {
        container.error = msg
    }

    private fun navigateToYourMeasuresForm() {
        if (!product.isNullOrEmpty() && data != null && !data!!.measurements.isNullOrEmpty()) {
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, YourMeasuresFormFragment.newInstance(
                    product!!,
                    user!!,
                    data!!,
                    selectedSex!!,
                ))
                .addToBackStack(null)
                .commit()
        }
    }

    private fun navigateToLastStepForm() {
        val request = getSaveRecommendRequest()
        val fragment = LastStepFormFragment.newInstance(
            product!!,
            data!!,
            request,
            false,
            selectedSex,
        )

        requireActivity().supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun navigateToRecommend(){

        val request = getSaveRecommendRequest()
        val fragment = ResultsFragment.newInstance(
                product!!,
                request,
                data!!
        )
        requireActivity().supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit()
    }

    private fun getSaveRecommendRequest():SaveRecommendRequest{
        val request = SaveRecommendRequest()

        request.sk = data?.sk
        request.ck = data?.ck
        request.sex = selectedSex ?: data?.sex
        request.user = user
        request.fit = sbFitLooseControl.progress
        request.w = RecommendationWay.BASIC_WAY

        if (isMetric){
            request.age = tiEtAgeMainMetric.text.toString().toInt()
            request.weight = if (weightKg > 0) weightKg.toString() else tiEtWeightMetric.text.toString().toFloat().toString()
            request.height = if (heightCm > 0) heightCm.toString() else tiEtHeightMetric.text.toString().toFloat().toString()
            request.metricSystem = MetricSystem.METRIC
            request.massSystem = MetricSystem.METRIC
        }else{
            request.age = tiEtAgeMainImperial.text.toString().toInt()
            request.weight = if (weightLbs > 0) weightLbs.toString() else tiEtWeightImperial.text.toString().toFloat().toString()
            request.height = if (heightTotalInches > 0) heightTotalInches.toString() else (tiEtFeetImperial.text.toString().toFloat() * 12 + tiEtInchesImperial.text.toString().toFloat()).toString()
            request.metricSystem = MetricSystem.IMPERIAL
            request.massSystem = MetricSystem.IMPERIAL
        }
        return request
    }

    private fun updateSubjectData() {
        subjectDataMetric = DataExtractionUtils.getSubjectDataBySex(data?.data?.dataMetric!!, data?.sex!!, selectedSex)
        subjectDataImperial = DataExtractionUtils.getSubjectDataBySex(data?.data?.dataImperial!!, data?.sex!!, selectedSex)
    }

    private fun updateDataAndViewsOnSexChange() {
        updateSubjectData()
        initMeasuresValues()
        setDefaultFormValues()
        initSeekBar()
    }
}