package com.usizy.sizebutton.ui.fragments

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.gson.Gson
import com.usizy.sizebutton.R
import com.usizy.sizebutton.events.RecommendSizeEvent
import com.usizy.sizebutton.utils.ShoeStringHelper
import com.usizy.sizequeryandroid.SizeQuery
import com.usizy.sizequeryandroid.SizeQueryException
import com.usizy.sizequeryandroid.SizeResult
import com.usizy.sizequeryandroid.model.PopupData
import com.usizy.sizequeryandroid.model.SaveRecommendRequest
import com.usizy.sizequeryandroid.model.SaveRecommendResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import kotlin.coroutines.CoroutineContext
import kotlin.collections.MutableSet


private const val ARG_DATA = "ARG_DATA"
private const val ARG_POPUP_DATA = "ARG_POPUP_DATA"
private const val ARG_PRODUCT = "ARG_PRODUCT"

/**
 * Use the [ResultsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ResultsFragment : Fragment(), CoroutineScope {

    private val DEFAULT_VALUE = 999
    private var request: SaveRecommendRequest? = null
    private var saveRecommendResponse: SaveRecommendResponse? = null
    private var popUpData: PopupData? = null
    private var product: String? = null

    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main
    private lateinit var requester: SizeQuery

    private lateinit var rootView: View
    private lateinit var tvRecommendSizeInfo : TextView
    private lateinit var pbRecommended : ProgressBar
    private lateinit var btnRecommendFinish : Button
    private lateinit var btnRecommendStart : Button
    private lateinit var radioGroup: RadioGroup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            product = it.getString(ARG_PRODUCT)
            request = Gson().fromJson(it.getString(ARG_DATA), SaveRecommendRequest::class.java)
            popUpData = Gson().fromJson(it.getString(ARG_POPUP_DATA), PopupData::class.java)
            it.getString(ARG_DATA)?.let { it1 -> Log.d("REQUEST", it1) }
        }

        job = Job()
        requester = SizeQuery(requireActivity())
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_results, container, false)

        initViews(rootView)

        return rootView
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param product productId.
         * @param request SaveRecommendRequest.
         * @param data PopupData
         * @return A new instance of fragment ResultsFragment.
         */
        @JvmStatic
        fun newInstance(product: String, request: SaveRecommendRequest, popupData: PopupData) =
                ResultsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_DATA, Gson().toJson(request))
                    putString(ARG_POPUP_DATA, Gson().toJson(popupData))
                    putString(ARG_PRODUCT, product)
                }
            }
    }

    override fun onStart() {
        super.onStart()

        fetchRecommend()
    }

    private fun fetchRecommend(){
        if (product != null && request != null){
            launch {
                try {
                    val response = requester.saveRecommend(product!!, request!!)
                    saveRecommendResponse = response
                    if (response.status != "fail") {
                        when (response.usizySizeResult) {
                            SizeResult.MATCH -> showRecommended(response)
                            SizeResult.UNDERFLOW -> navigateToError(getString(R.string.UNDERFLOW))
                            SizeResult.OVERFLOW -> navigateToError(getString(R.string.OVERFLOW))
                            else -> navigateToError(null)
                        }
                    } else {
                        navigateToError("Request failed")
                    }
                } catch (e: SizeQueryException) {
                    e.printStackTrace()
                    navigateToError(null)
                }
            }
        }else{
            navigateToError(null)
        }
    }

    @SuppressLint("ResourceType")
    private fun showRecommended(response: SaveRecommendResponse){
        pbRecommended.visibility = View.GONE
        tvRecommendSizeInfo.visibility = View.VISIBLE

        btnRecommendStart.isEnabled = true
        btnRecommendFinish.isEnabled = true

        if(product?.contains("SHOES") == true) {
            tvRecommendSizeInfo.text = ShoeStringHelper.formatShoeResultString(response.usizySizeCountry, ignoreCountrySystem = false)
        } else {
            tvRecommendSizeInfo.text = response.usizySizeCountry
        }
        if (response.extra != null && response.extra.isNotEmpty()){
            val keys = response.extra.keys.toTypedArray()
            val values = response.extra.values.toTypedArray()

            val extraSet: MutableSet<String> = mutableSetOf(response.usizySizeCountrySystem)

            keys.forEachIndexed { index, key ->
                if (!extraSet.contains(key)) {
                    val radioButton = RadioButton(rootView.context)
                    radioButton.layoutParams = getRadioButtonLayout()
                    radioButton.text = key
                    addTintColor(radioButton)
                    radioButton.id = index
                    radioGroup.addView(radioButton)
                    extraSet.add(key)
                }
            }

            val radioButton = RadioButton(rootView.context)
            radioButton.layoutParams = getRadioButtonLayout()
            radioButton.text = response.usizySizeCountrySystem
            radioButton.id = DEFAULT_VALUE
            addTintColor(radioButton)
            radioGroup.addView(radioButton)
            radioGroup.check(DEFAULT_VALUE)

            radioGroup.setOnCheckedChangeListener { _, checkedId ->
                if (checkedId == DEFAULT_VALUE){
                    tvRecommendSizeInfo.text = response.usizySizeCountry
                }else{
                    tvRecommendSizeInfo.text = values[checkedId]
                }
            }

            radioGroup.visibility = View.VISIBLE
        }
    }

    private fun getRadioButtonLayout(): LinearLayout.LayoutParams {
        return LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    private fun addTintColor(radioButton: RadioButton){
        if (Build.VERSION.SDK_INT >= 21) {
            val colorStateList = ColorStateList(
                arrayOf(
                    intArrayOf(-android.R.attr.state_checked),
                    intArrayOf(android.R.attr.state_checked)
            ),
                intArrayOf(
                    ContextCompat.getColor(requireContext(),R.color.colorPrimary),
                    ContextCompat.getColor(requireContext(),R.color.colorPrimary),
                )
            )
            radioButton.buttonTintList = colorStateList
            radioButton.invalidate()
        }
    }

    private fun initViews(it: View) {
        tvRecommendSizeInfo = it.findViewById(R.id.tvRecommendSizeInfo)
        pbRecommended = it.findViewById(R.id.pbRecommended)
        btnRecommendFinish  = it.findViewById(R.id.btnRecommendFinish)
        btnRecommendStart  = it.findViewById(R.id.btnRecommendStart)
        radioGroup = it.findViewById(R.id.radioGroup)

        btnRecommendFinish.setOnClickListener(View.OnClickListener {
            sendRecommendSizeEvent()
            requireActivity().finish()
        })

        btnRecommendStart.setOnClickListener(View.OnClickListener {
            navigateToStart()
        })
    }

    private fun sendRecommendSizeEvent(){
        val event = RecommendSizeEvent()
        event.productId = product
        event.response = saveRecommendResponse
        if (product?.contains("SHOES") == true) {
            val usizySizeCountry = saveRecommendResponse?.let { ShoeStringHelper.formatShoeResultString(it.usizySizeCountry, ignoreCountrySystem = true) }.toString()
            event.response?.usizySize = "${saveRecommendResponse?.usizyQuicksizeText} $usizySizeCountry"
        }
        val eventBus = EventBus.getDefault()
        eventBus.post(event)
    }

    private fun navigateToError(text: String?) {
        requireActivity().supportFragmentManager.popBackStack(
                null,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
        )

        requireActivity().supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(R.id.fragment_container, ErrorFragment.newInstance(text))
                .addToBackStack(null)
                .commit()
    }

    private fun navigateToStart() {
        requireActivity().supportFragmentManager.popBackStack(
                null,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
        )
    }
}