package com.usizy.sizebutton.ui.views.checkables

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Checkable
import android.widget.ImageView
import androidx.core.content.res.use
import androidx.core.view.children
import com.usizy.sizebutton.R

open class CheckableImageSelectorButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ImageSelectorButton(context, attrs, defStyleAttr), Checkable {

    interface OnCheckedChangeListener {
        fun onCheckedChanged(view: View, isChecked: Boolean)
    }

    private val listeners = mutableListOf<OnCheckedChangeListener>()

    private var checked: Boolean = false

    init {
        context.theme.obtainStyledAttributes(attrs, R.styleable.IndicatorButton, 0, 0).use {
            isChecked =
                it.getBoolean(R.styleable.CheckableIndicatorButton_android_checked, isChecked)
        }
    }

    fun addOnCheckChangeListener(onCheckedChangeListener: OnCheckedChangeListener) {
        listeners.add(onCheckedChangeListener)
    }

    fun removeOnCheckChangeListener(onCheckedChangeListener: OnCheckedChangeListener) {
        listeners.remove(onCheckedChangeListener)
    }

    override fun isChecked() = checked

    override fun toggle() {
        isChecked = !checked
    }

    override fun setChecked(checked: Boolean) {
        if (this.checked != checked) {
            this.checked = checked
            children.filter { it is Checkable }.forEach {
                (it as Checkable).isChecked = checked

            }
            listeners.forEach { it.onCheckedChanged(this, this.checked) }
            val ivFigure = findViewById<ImageView>(R.id.iv_figure)
            val ivCheckIcon = findViewById<ImageView>(R.id.iv_check_icon)
            if (checked) {
                ivCheckIcon.visibility = View.VISIBLE
                ivFigure.alpha = 1.0f
            } else {
                ivCheckIcon.visibility = View.GONE
                ivFigure.alpha = 0.5f
            }
            refreshDrawableState()
        }
    }

    override fun performClick(): Boolean {
        toggle()
        return super.performClick()
    }

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        val drawableState = super.onCreateDrawableState(extraSpace + 1)
        if (isChecked) {
            View.mergeDrawableStates(drawableState, CHECKED_STATE_SET)
        }
        return drawableState
    }

    companion object {
        private val CHECKED_STATE_SET = intArrayOf(android.R.attr.state_checked)
    }
}