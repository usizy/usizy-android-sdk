package com.usizy.sizebutton.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.usizy.sizebutton.R

/**
 * Use the [LoaderFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoaderFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_loader, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment LoaderFragment.
         */
        @JvmStatic
        fun newInstance() =
            LoaderFragment().apply {
                arguments = Bundle().apply {}
            }
    }
}