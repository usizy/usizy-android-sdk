package com.usizy.sizebutton.ui

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.usizy.sizebutton.R
import com.usizy.sizebutton.constants.*
import com.usizy.sizebutton.ui.dialog.PrivacyDialog
import com.usizy.sizebutton.ui.fragments.ErrorFragment
import com.usizy.sizebutton.ui.fragments.LoaderFragment
import com.usizy.sizebutton.ui.fragments.MainFormFragment
import com.usizy.sizebutton.ui.fragments.ShoesFormFragment
import com.usizy.sizequeryandroid.SizeQuery
import com.usizy.sizequeryandroid.SizeQueryException
import com.usizy.sizequeryandroid.model.PopupData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext
import com.usizy.sizequeryandroid.PopupZones

class FormActivity : AppCompatActivity(), CoroutineScope {

    private lateinit var activity: FormActivity
    private lateinit var requester :SizeQuery
    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var productId: String = "BABY_UPPER"
    private var logoResId: Int = R.drawable.logo_usizy_v1
    private var user: String = "AnonymousUser"
    private var logoUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)

        activity = this
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        supportActionBar?.hide()

        getIntentExtras()

        initializeRequester()
        initializePrivacyButton()
        initializeLogo()
        initializeCloseButton()

        fetchPopupData()
    }

    private fun replaceFragment(fragment: Fragment, frame: Int) {
        if (!activity.supportFragmentManager.isDestroyed){
            val transaction = activity.supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(frame, fragment)
            transaction.commit()
        }
    }

    private fun initializeRequester(){
        job = Job()
        requester = SizeQuery(
            this,
            user
        )
    }

    private fun initializePrivacyButton(){
        val btnPrivacy = findViewById<Button>(R.id.btnPrivacy)
        btnPrivacy.setOnClickListener {
            PrivacyDialog().show(this)
        }
    }

    private fun initializeLogo(){
        val ivLogo = findViewById<ImageView>(R.id.ivLogo)
        if (!logoUrl.isNullOrBlank()) {
            Glide.with(this).load(logoUrl).into(ivLogo)
        } else {
            ivLogo.setImageResource(logoResId);
        }
    }

    private fun initializeCloseButton(){
        (findViewById<View>(R.id.close))
            .setOnClickListener(View.OnClickListener {
            finish()
        })
    }

    private fun getIntentExtras(){
        if (intent.getStringExtra(PRODUCT_DATA_KEY) != null){
            productId = intent.getStringExtra(PRODUCT_DATA_KEY).toString()
        }

        if (intent.getStringExtra(USER_RES_ID_KEY) != null){
            user = intent.getStringExtra(USER_RES_ID_KEY).toString()
        }

        val customLogoResId = intent.getIntExtra(LOGO_RES_ID_KEY, -1)
        if (customLogoResId != -1){
            logoResId = customLogoResId
        }
    }

    private fun showLoader(){
        replaceFragment(
            LoaderFragment.newInstance(),
            R.id.fragment_container)
    }

    private fun fetchPopupData(){
        showLoader()

        launch {
            try {
                val data: PopupData = requester.openPopup(productId)
                logoUrl = data.logo

                if(data.zone == PopupZones.SHOES) {
                    replaceFragment(
                        ShoesFormFragment.newInstance(productId, user, data),
                        R.id.fragment_container)
                } else {
                    replaceFragment(
                        MainFormFragment.newInstance(productId, user, data),
                        R.id.fragment_container)
                }
            } catch (e: SizeQueryException) {
                e.printStackTrace()

                replaceFragment(
                    ErrorFragment.newInstance(),
                    R.id.fragment_container)
            }
        }
    }
}