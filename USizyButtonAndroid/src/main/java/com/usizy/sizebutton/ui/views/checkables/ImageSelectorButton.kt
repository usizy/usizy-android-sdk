package com.usizy.sizebutton.ui.views.checkables

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.use
import androidx.core.view.children
import com.usizy.sizebutton.R

open class ImageSelectorButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var value = -1

    init {
        LayoutInflater.from(context).inflate(R.layout.indicator_button, this, true)
        val ivFigure = findViewById<ImageView>(R.id.iv_figure)
        val tvMeasure = findViewById<TextView>(R.id.tv_measure)
        val ivCheckIcon = findViewById<ImageView>(R.id.iv_check_icon)

        context.theme.obtainStyledAttributes(attrs, R.styleable.IndicatorButton, 0, 0).use {
            isEnabled = it.getBoolean(R.styleable.IndicatorButton_android_enabled, isEnabled)
            ivFigure.setImageDrawable(it.getDrawable(R.styleable.IndicatorButton_android_src))
            tvMeasure.text =
                it.getText(R.styleable.IndicatorButton_android_text).toString().replaceFirstChar(Char::titlecase)
            value = it.getInt(R.styleable.IndicatorButton_android_value, -1)
        }
        ivCheckIcon.visibility = View.GONE
        ivFigure.alpha = 0.7f
        isClickable = true
        isFocusable = true
    }

    fun getValue(): Int {
        return value
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        children.forEach { it.isEnabled = enabled }
    }
}