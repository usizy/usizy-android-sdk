package com.usizy.sizebutton.ui.views.checkables

import android.content.Context
import android.util.AttributeSet

class RadioImageSelectorButton @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : CheckableImageSelectorButton(context, attrs, defStyleAttr){

    override fun performClick(): Boolean {
        if (isChecked) return false
        return super.performClick()
    }
}