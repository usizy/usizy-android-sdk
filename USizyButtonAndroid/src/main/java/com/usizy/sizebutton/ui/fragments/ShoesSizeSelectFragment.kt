package com.usizy.sizebutton.ui.fragments

import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.view.get
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.usizy.sizebutton.R
import com.usizy.sizebutton.constants.METRIC_VALUE
import com.usizy.sizebutton.ui.adapters.ShoeSizesAdapter
import com.usizy.sizebutton.utils.DataExtractionUtils
import com.usizy.sizequeryandroid.RecommendationWay
import com.usizy.sizequeryandroid.model.*

private const val ARG_PRODUCT = "product"
private const val ARG_USER = "user"
private const val ARG_DATA = "data"
private const val ARG_SHOE_FAV = "shoeFav"

/**
 * A simple [Fragment] subclass.
 * Use the [ShoesSizeSelectFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ShoesSizeSelectFragment : Fragment() {
    private var product: String? = null
    private var user: String? = null
    private var data: PopupData? = null
    private var shoeFav: ShoeFavs? = null

    private var shoeBrand: Int? = null
    private var shoeSize: String? = null
    private var shoeFavSizes: ArrayList<ShoeSizes>? = null
    private var sizesKeys: ArrayList<String>? = null
    private var defaultShoeSystem: String? = null
    private var subjectDataMetric: SubjectData? = null
    private var subjectDataImperial: SubjectDataImperial? = null

    private var selectedSex: String? = null

    // WidgetRefs
    private lateinit var ivBrandImage: ImageView
    private lateinit var tvBrandText: TextView
    private lateinit var rgSizeRegion: RadioGroup
    private lateinit var gvShoeSizes: GridView
    private lateinit var btnGoBack: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            product = it.getString(ARG_PRODUCT)
            user = it.getString(ARG_USER)
            data = Gson().fromJson(it.getString(ARG_DATA), PopupData::class.java)
            shoeFav = Gson().fromJson(it.getString(ARG_SHOE_FAV), ShoeFavs::class.java)
        }

        shoeBrand = shoeFav?.id
        sizesKeys = ArrayList()
        shoeFavSizes = ArrayList()
        defaultShoeSystem = data?.shoeSystem
        updateSubjectData()
        initSizesData()
    }

    private fun updateSubjectData() {
        subjectDataMetric = DataExtractionUtils.getSubjectDataBySex(data?.data?.dataMetric!!, data?.sex!!, selectedSex)
        subjectDataImperial = DataExtractionUtils.getSubjectDataBySex(data?.data?.dataImperial!!, data?.sex!!, selectedSex)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_shoes_size_select, container, false)
        initViews(rootView)
        setData()
        return rootView
    }

    private fun initBrandImageView(it: View) {
        ivBrandImage = it.findViewById(R.id.iv_brand_image)
        if (!shoeFav?.thumb.isNullOrEmpty()) {
            Glide.with(this).load(shoeFav?.thumb).into(ivBrandImage)
        }
    }

    private fun initBrandTextView(it: View) {
        tvBrandText = it.findViewById(R.id.tv_brand_text)
        tvBrandText.text = shoeFav?.text
    }

    private fun initRegionRadioGroup(it: View) {
        rgSizeRegion = it.findViewById(R.id.rg_size_region)

        shoeFavSizes?.forEachIndexed { index, size ->
            val radioButton = RadioButton(it.context)
            radioButton.text = size.region
            addTintColor(radioButton)
            radioButton.id = index

            rgSizeRegion.addView(radioButton)

            if (size.region == defaultShoeSystem) {
                rgSizeRegion.check(index)
            }
        }

        rgSizeRegion.setOnCheckedChangeListener { group, checkedId ->
            val radioButton = group[checkedId] as RadioButton
            val region = radioButton.text
            val size = shoeFavSizes?.find {
                it.region == region.toString()
            }
            val adapter = gvShoeSizes.adapter as ShoeSizesAdapter
            size?.values?.let { it1 -> adapter.update(it1) }
        }
    }

    private fun addTintColor(radioButton: RadioButton){
        if (Build.VERSION.SDK_INT >= 21) {
            val colorStateList = ColorStateList(
                arrayOf(
                    intArrayOf(-android.R.attr.state_checked),
                    intArrayOf(android.R.attr.state_checked)
                ),
                intArrayOf(
                    ContextCompat.getColor(requireContext(),R.color.colorPrimary),
                    ContextCompat.getColor(requireContext(),R.color.colorPrimary),
                )
            )
            radioButton.buttonTintList = colorStateList
            radioButton.invalidate()
        }
    }

    private fun initSizesGridView(it: View) {
        gvShoeSizes = it.findViewById(R.id.gv_shoe_sizes)
        gvShoeSizes.setOnItemClickListener { _, _, i, _ ->
            shoeSize =  (gvShoeSizes.adapter as ShoeSizesAdapter).getItem(i)
            navigateToRecommend()
        }
    }

    private fun initBtnGoBack(it: View) {
        btnGoBack = it.findViewById(R.id.btn_go_back_sizes)
        btnGoBack.setOnClickListener{
            requireActivity().supportFragmentManager.popBackStack()
        }
    }

    private fun initViews(it: View) {
        initBrandImageView(it)
        initBrandTextView(it)
        initRegionRadioGroup(it)
        initSizesGridView(it)
        initBtnGoBack(it)
    }

    private fun initSizesData() {
        val sizes = shoeFav?.sizes as Map<*, *>
        for(size in sizes) {
            val shoeSize = ShoeSizes(size.key as String, size.value as ArrayList<ArrayList<*>>)
            shoeFavSizes?.add(shoeSize)
        }
    }

    private fun setData() {
        val adapter = shoeFavSizes?.get(0)?.let { it.values?.let { it1 -> ShoeSizesAdapter(it1) } }
        gvShoeSizes.adapter = adapter
    }

    private fun navigateToRecommend() {
        if (product == null || data == null) return

        val request = getSaveRecommendRequest()
        val fragment = ResultsFragment.newInstance(
                product!!,
                request,
                data!!
        )
        requireActivity().supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit()
    }

    private fun realSize(shoeSize: String?): String? {
        val radioButton = rgSizeRegion[rgSizeRegion.checkedRadioButtonId] as RadioButton
        val region = radioButton.text
        val favSizes = shoeFavSizes?.find {
            it.region == region.toString()
        }
        for (size in favSizes?.values!!) {
            if (size[0] == shoeSize) {
                val tmp: ArrayList<String> = size[1] as ArrayList<String>
                return tmp.last()
            }
        }
        return shoeSize
    }

    private fun getSaveRecommendRequest(): SaveRecommendRequest{
        val request = SaveRecommendRequest()

        if (data?.metricSystem == METRIC_VALUE) {
            request.age = subjectDataMetric?.age
        } else {
            request.age = subjectDataImperial?.age
        }
        request.ck = data?.ck
        request.footType = subjectDataMetric?.footType
        request.metricSystem = data?.metricSystem
        request.w = RecommendationWay.BASIC_WAY
        request.shoeBrand = shoeBrand
        request.shoeSize = this.realSize(shoeSize)
        request.sex = data?.sex

        return request
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param product productId.
         * @param data PopupData.
         * @param user user string
         * @param shoeFav ShoeFavs
         *
         * @return A new instance of fragment ShoesSizeSelectFragment.
         */
        @JvmStatic
        fun newInstance(product: String, user: String, data: PopupData, shoeFav: ShoeFavs) =
            ShoesSizeSelectFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_DATA, Gson().toJson(data) )
                    putString(ARG_SHOE_FAV, Gson().toJson(shoeFav) )
                    putString(ARG_PRODUCT, product)
                    putString(ARG_USER, user)
                }
            }
    }

}