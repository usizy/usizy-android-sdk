package com.usizy.sizebutton.ui.fragments

import com.usizy.sizebutton.ui.adapters.ShoeFavsAdapter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.usizy.sizebutton.R
import com.usizy.sizequeryandroid.SexValues
import com.usizy.sizequeryandroid.SizeQuery
import com.usizy.sizequeryandroid.SizeQueryException
import com.usizy.sizequeryandroid.model.PopupData
import com.usizy.sizequeryandroid.model.ShoeBrandRequest
import com.usizy.sizequeryandroid.model.ShoeFavs
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

private const val ARG_DATA = "ARG_DATA"
private const val ARG_PRODUCT = "ARG_PRODUCT"
private const val ARG_USER = "ARG_USER"

/**
 * A simple [Fragment] subclass.
 * Use the [ShoesFormFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ShoesFormFragment : Fragment(), CoroutineScope {
    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main
    private lateinit var requester: SizeQuery

    private var product: String? = null
    private var data: PopupData? = null
    private var user: String? = null

    private lateinit var gvShoeFavs: GridView
    private lateinit var etSearch: EditText
    private lateinit var iv_product_main: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            product = it.getString(ARG_PRODUCT)
            data = Gson().fromJson(it.getString(ARG_DATA), PopupData::class.java)
            user = it.getString(ARG_USER)

            job = Job()
            requester = SizeQuery(requireActivity())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_shoes_form, container, false)
        initViews(rootView)
        setData()
        return rootView
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param product productId.
         * @param data PopupData.
         * @param user user string
         * @return A new instance of fragment ShoesFormFragment.
         */
        @JvmStatic
        fun newInstance(product: String, user: String, data: PopupData) =
            ShoesFormFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_DATA, Gson().toJson(data) )
                    putString(ARG_PRODUCT, product)
                    putString(ARG_USER, user)
                }
            }
    }

    private val searchTextWatcher = object :TextWatcher{
        private var searchFor = ""

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val searchText = s.toString().trim()
            if (searchText == searchFor)
                return

            searchFor = searchText

            launch {
                delay(300)  //debounce timeOut
                if (searchText != searchFor)
                    return@launch

                fetchData(searchText)
            }
        }

        override fun afterTextChanged(s: Editable?) = Unit
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
    }

    private fun initGrid(it: View){
        gvShoeFavs = it.findViewById(R.id.gv_shoe_favs)
        gvShoeFavs.setOnItemClickListener { _, _, i, _ ->
            val item =  (gvShoeFavs.adapter as ShoeFavsAdapter).getItem(i) as ShoeFavs
            navigateToShoesSizeSelect(item)
        }
    }

    private fun navigateToShoesSizeSelect(shoeFav: ShoeFavs) {
        if (!product.isNullOrEmpty() && data != null && !data!!.measurements.isNullOrEmpty()) {
            requireActivity().supportFragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                    .replace(R.id.fragment_container, ShoesSizeSelectFragment.newInstance(
                            product!!,
                            user!!,
                            data!!,
                            shoeFav,
                    ))
                    .addToBackStack(null)
                    .commit()
        }
    }

    private fun initImageProduct(it: View){
        iv_product_main = it.findViewById(R.id.iv_product_main)
        if (!data?.image.isNullOrBlank()) {
            Glide.with(this).load(data?.image).into(iv_product_main)
            iv_product_main.visibility = View.VISIBLE
        }
    }

    private fun initSearchEditText(it: View){
        etSearch = it.findViewById(R.id.etSearch)
        etSearch.addTextChangedListener(searchTextWatcher)
    }

    private fun initMeasuresButton(it: View){
        (it.findViewById<View>(R.id.tv_to_basic_measures))
            .setOnClickListener(View.OnClickListener {
                navigateToYourMeasuresForm()
            })
    }

    private fun initViews(it: View) {
        initGrid(it)
        initImageProduct(it)
        initSearchEditText(it)
        initMeasuresButton(it)
    }

    private fun setData() {
        var adapter = ShoeFavsAdapter(data!!.shoeFavs)
        gvShoeFavs.adapter = adapter
    }

    private fun fetchData(searchText: String){

        launch {
            try {
                val request = ShoeBrandRequest()
                request.q = searchText
                request.sex = if (data?.sex === SexValues.FEMALE) data?.sex else SexValues.MALE

                val response = requester.shoeBrand(product!!, request)
                (gvShoeFavs.adapter as ShoeFavsAdapter).update(response.shoeFavs)
            } catch (e: SizeQueryException) {
                e.printStackTrace()
                (gvShoeFavs.adapter as ShoeFavsAdapter).update(data!!.shoeFavs)
            }
        }
    }

    private fun navigateToYourMeasuresForm() {
        if (!product.isNullOrEmpty() && data != null && !data!!.measurements.isNullOrEmpty()) {
            requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, ShoesMeasuresFragment.newInstance(
                            product!!,
                            user!!,
                            data!!
                    ))
                    .addToBackStack(null)
                    .commit()
        }
    }
}