package com.usizy.sizebutton.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import com.usizy.sizebutton.R

private const val ARG_TEXT = "ARG_TEXT"

class ErrorFragment : Fragment() {
    private var text: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            text = it.getString(ARG_TEXT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_error, container, false)

        if (text != null){
            val tvMessage = rootView.findViewById<TextView>(R.id.tv_message)
            tvMessage.text = text
        }

        (rootView.findViewById<Button>(R.id.btnRecommendStart)).setOnClickListener(View.OnClickListener {
            navigateToStart()
        })

        return rootView
    }

    private fun navigateToStart() {
        requireActivity().supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    companion object {
        @JvmStatic
        fun newInstance(text: String?) =
            ErrorFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_TEXT, text)
                }
            }

        @JvmStatic
        fun newInstance() =
            ErrorFragment().apply {
                arguments = Bundle().apply {}
            }
    }
}