package com.usizy.sizebutton.utils

import com.usizy.sizequeryandroid.SexValues
import com.usizy.sizequeryandroid.model.DataImperial
import com.usizy.sizequeryandroid.model.DataMetric
import com.usizy.sizequeryandroid.model.SubjectData
import com.usizy.sizequeryandroid.model.SubjectDataImperial

object DataExtractionUtils {
    /**
     * Metric overload version
     *
     * @param dataMetric The data to extract the SubjectData
     * @param sex filter for the SubjectData to retrieve
     * @param selectedSex if there is a select of human sex or of unisex sex, send the selected
     *                    sex to filter the resultant SubjectData
     *
     * @return a SubjectData object if the sex filters matches, null otherwise
     */
    fun getSubjectDataBySex(dataMetric: DataMetric, sex: String, selectedSex: String?): SubjectData? {
        return when (sex) {
            SexValues.BABY -> dataMetric.baby!!
            SexValues.CHILD -> dataMetric.child!!
            SexValues.BOY -> dataMetric.boy!!
            SexValues.GIRL -> dataMetric.girl!!
            SexValues.FEMALE -> dataMetric.female!!
            SexValues.MALE -> dataMetric.male!!
            SexValues.HUMAN -> if ( selectedSex != null ) getSexForHumanType(selectedSex, dataMetric)
                                    else dataMetric.male!!
            SexValues.UNISEX -> if ( selectedSex != null ) getSexForUnisexType(selectedSex, dataMetric)
                                    else dataMetric.male!!
            else -> null
        }
    }

    private fun getSexForHumanType(selectedSex: String, dataMetric: DataMetric): SubjectData {
        return if (selectedSex == SexValues.BOY) dataMetric.boy!! else dataMetric.male!!
    }

    private fun getSexForUnisexType(selectedSex: String, dataMetric: DataMetric): SubjectData {
        return if (selectedSex == SexValues.FEMALE) dataMetric.female!! else dataMetric.male!!
    }

    /**
     * Imperial metric overload version
     *
     * @param dataMetricImperial The data to extract the SubjectDataImperial
     * @param sex filter for the SubjectData to retrieve
     * @param selectedSex if there is a select of human sex or of unisex sex, send the selected
     *                    sex to filter the resultant SubjectData
     *
     * @return a SubjectDataImperial object if the sex filters matches, null otherwise
     */
    fun getSubjectDataBySex(dataMetricImperial: DataImperial, sex: String, selectedSex: String?): SubjectDataImperial? {
        return when (sex) {
            SexValues.BABY -> dataMetricImperial.baby!!
            SexValues.CHILD -> dataMetricImperial.child!!
            SexValues.BOY -> dataMetricImperial.boy!!
            SexValues.GIRL -> dataMetricImperial.girl!!
            SexValues.FEMALE -> dataMetricImperial.female!!
            SexValues.MALE -> dataMetricImperial.male!!
            SexValues.HUMAN -> if ( selectedSex != null ) getSexForHumanType(selectedSex, dataMetricImperial)
                                else dataMetricImperial.male!!
            SexValues.UNISEX -> if ( selectedSex != null ) getSexForUnisexType(selectedSex, dataMetricImperial)
                                else dataMetricImperial.male!!
            else -> null
        }
    }

    private fun getSexForHumanType(selectedSex: String, dataMetricImperial: DataImperial): SubjectDataImperial {
        return if (selectedSex == SexValues.BOY) dataMetricImperial.boy!! else dataMetricImperial.male!!
    }

    private fun getSexForUnisexType(selectedSex: String, dataMetricImperial: DataImperial): SubjectDataImperial {
        return if (selectedSex == SexValues.FEMALE) dataMetricImperial.female!! else dataMetricImperial.male!!
    }
}