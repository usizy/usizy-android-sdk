package com.usizy.sizebutton.utils

import com.usizy.sizequeryandroid.model.HeightImperial
import kotlin.math.abs
import kotlin.math.floor

object MetricsConvertHelpers {

    private const val conversionFactorA = 2.54f
    private const val conversionFactorB = 12
    private const val conversionWeightFactor= 2.205f

    fun fromFeetInchesToCm (feet: Float, inches: Float): Float {
        val totalInches = feet * 12 + inches
        return totalInches * conversionFactorA
    }

    fun getTotalInchesFromCm(cm: Float): Float {
        return cm / conversionFactorA
    }

    fun fromInchesToCm(inches: Float): Float {
        return inches * conversionFactorA
    }

    fun getTotalInchesFromImperial(heightImperial: HeightImperial): Float {
        return heightImperial.ft * conversionFactorB + heightImperial.inX
    }

    fun getImperialFromTotalInches (totalInches: Float): HeightImperial {
        val feet = floor(totalInches / conversionFactorB)
        val inX = totalInches % conversionFactorB
        return HeightImperial(feet, inX)
    }

    fun fromKgToPounds(kgs: Float): Float {
        return kgs * conversionWeightFactor
    }

    fun fromPoundsToKgs(pounds: Float): Float {
        return pounds / conversionWeightFactor
    }

    fun getDecimalPart(num: Float): Float {
        return abs(num - floor(num).toInt())
    }
}