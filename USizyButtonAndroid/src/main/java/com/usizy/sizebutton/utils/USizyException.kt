package com.usizy.sizebutton.utils

class USizyException(message: String) : Exception(message)