package com.usizy.sizebutton.utils

import android.os.Build
import android.os.LocaleList
import java.util.Locale

class LocaleUtils {
    companion object {
        fun getCurrentLanguage(): String? {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                LocaleList.getDefault()[0].language
            } else {
                Locale.getDefault().language
            }
        }
    }

}