package com.usizy.sizebutton.utils

import android.view.View
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.usizy.sizebutton.constants.METRIC_VALUE

class InputShape(
    var container: TextInputLayout? = null,
    var input: TextInputEditText? = null,
    var max: Float = 120f,
    var min: Float = 20f,
    var type: String = METRIC_VALUE,
    var isInputFormHolderVisible: Boolean = true,
    var inputVisible: Int = View.VISIBLE,
    var skipListener: Boolean = false,
    var isImperialHeight: Boolean = false
)
