package com.usizy.sizebutton.utils

import android.util.Log

object ShoeStringHelper {
    private val twoThirdSizes: Array<Int>
        get() = arrayOf(36, 40, 44, 48, 52, 54, 55)
    private const val DELIMITER_BLANK_SPACE = " "

    fun formatUnicodeCharsList(list: ArrayList<ArrayList<*>>): List<String> {
        return list.map {
            var size: String = it[0] as String
            val splitStr = size.split(DELIMITER_BLANK_SPACE)
            if (splitStr.count() > 1) {
                val sizeNum = splitStr[0].toInt()

                var unicodeFraction: Int
                // U+2153 0x2153 1/3
                val oneThirdUnicode = 0x2153
                // U+2154 0x2154 2/3
                val twoThirdsUnicode = 0x2154

                unicodeFraction = when (sizeNum) {
                    in twoThirdSizes -> twoThirdsUnicode
                    else -> oneThirdUnicode
                }

                val fractionStr = String(Character.toChars(unicodeFraction))

                size = "${ splitStr[0] } $fractionStr"
            }
            return@map size
        }
    }

    fun formatShoeResultString(result: String, ignoreCountrySystem: Boolean): String {
        val splitStrArray = result.split(DELIMITER_BLANK_SPACE)
        var formatted = result
        // if it has fraction the length of the array will be 3
        if (splitStrArray.count() > 2) {
            val sizeNum = splitStrArray[1].toInt()

            var unicodeFraction: Int
            // U+2153 0x2153 1/3
            val oneThirdUnicode = 0x2153
            // U+2154 0x2154 2/3
            val twoThirdsUnicode = 0x2154

            unicodeFraction = when (sizeNum) {
                in twoThirdSizes -> twoThirdsUnicode
                else -> oneThirdUnicode
            }

            val fractionStr = String(Character.toChars(unicodeFraction))

            if (ignoreCountrySystem) {
                formatted = "${splitStrArray[1]} $fractionStr"
            } else {
                formatted = "${splitStrArray[0]} ${splitStrArray[1]} $fractionStr"
            }

        }
        return formatted
    }

}