package com.usizy.sizebutton.utils

import android.view.View
import com.usizy.sizebutton.constants.METRIC_VALUE
import kotlin.math.roundToInt

object FormValidationHelpers {
    fun isArrayInputsValid(inputsList: List<InputShape>, checkForVisibleInputs: Boolean = false): Boolean {
        var isValid = true
        val inputs = if (checkForVisibleInputs)
                inputsList.filter { it.isInputFormHolderVisible && it.inputVisible == View.VISIBLE}
            else
                inputsList.filter {it.isInputFormHolderVisible}

        for (inputShape in inputs) {
            val isOutOfRange = Integer.parseInt(inputShape.input!!.text.toString()) < inputShape.min || Integer.parseInt(
                    inputShape.input!!.text.toString()) > inputShape.max
            if (isOutOfRange) {
                inputShape.container!!.error = "${inputShape.min.roundToInt()} - ${inputShape.max.roundToInt()}"
            }
            if (!inputShape.container!!.error.isNullOrBlank()) isValid = false
        }
        return isValid
    }
}