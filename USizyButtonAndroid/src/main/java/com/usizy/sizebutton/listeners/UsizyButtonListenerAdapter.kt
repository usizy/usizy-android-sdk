package com.usizy.sizebutton.listeners

import com.usizy.sizequeryandroid.model.SaveRecommendResponse

interface UsizyButtonListenerAdapter {
    fun onRecommendedSize (productId:String?, data:SaveRecommendResponse?)
}

