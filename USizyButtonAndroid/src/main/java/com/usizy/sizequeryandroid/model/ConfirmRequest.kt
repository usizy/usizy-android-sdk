package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

open class MissingOrderId(message: String) : Exception(message)
open class MissingProductIds(message: String) : Exception(message)
open class MissingSizes(message: String) : Exception(message)

class ConfirmRequest(
    @SerializedName("order_id")
    var order_id: String,
    @SerializedName("product_ids")
    var product_ids: List<String>,
    @SerializedName("variation_ids")
    var variation_ids: List<String>? = null,
    @SerializedName("sizes")
    var sizes: List<String>,
    @SerializedName("sizes_system")
    var sizes_system: List<String>? = null,
    @SerializedName("currency")
    var currency: String? = null,
    @SerializedName("prices_vat")
    var prices_vat: List<String>? = null,
    @SerializedName("prices_no_vat")
    var prices_no_vat: List<String>? = null,
    @SerializedName("total_vat")
    var total_vat: String? = null,
    @SerializedName("total_no_vat")
    var total_no_vat: String? = null,
    @SerializedName("shipping_cost")
    var shipping_cost: String? = null,
) {
    @SerializedName("sk")
    var sk: String? = null
    @SerializedName("user")
    var user: String? = null
    @SerializedName("ref")
    var ref: String? = null
    @SerializedName("language")
    var language: String? = null

    init {
        if (order_id.isEmpty()) {
            throw MissingOrderId("")
        }
        if (product_ids.isEmpty()) {
            throw MissingProductIds("")
        }
        if (sizes.isEmpty()) {
            throw MissingSizes("")
        }
    }
}
