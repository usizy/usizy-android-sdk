package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

class ShoeSizes (
   val region: String?,
   val values: ArrayList<ArrayList<*>>?
)