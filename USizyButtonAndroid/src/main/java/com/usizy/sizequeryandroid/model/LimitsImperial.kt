package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

data class LimitsImperial(
        @SerializedName("age")
        val age: MeasureLimitMetric,
        @SerializedName("chest")
        val chest: MeasureLimitMetric,
        @SerializedName("height")
        val height: HeightLimitImperial,
        @SerializedName("hips")
        val hips: MeasureLimitMetric,
        @SerializedName("waist")
        val waist: MeasureLimitMetric,
        @SerializedName("weight")
        val weight: MeasureLimitMetric,
        @SerializedName("foot")
        val foot: MeasureLimitMetric,
)
