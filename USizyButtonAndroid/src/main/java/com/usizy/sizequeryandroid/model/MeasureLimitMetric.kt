package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

data class MeasureLimitMetric(
        @SerializedName("max")
        val max: Float,
        @SerializedName("min")
        val min: Float,
        @SerializedName("step")
        val step: Float
)
