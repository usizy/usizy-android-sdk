package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

data class HeightImperial(
    @SerializedName("ft")
    val ft: Float,
    @SerializedName("in")
    val inX: Float
)