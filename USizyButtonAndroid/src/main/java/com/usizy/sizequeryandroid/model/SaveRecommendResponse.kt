package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

data class SaveRecommendResponse(
    @SerializedName("branding")
    val branding: String,
    @SerializedName("extra")
    val extra: HashMap<String, String>?,
    @SerializedName("status")
    val status: String,
    @SerializedName("uid")
    val uid: String,
    @SerializedName("usizy_in_stock")
    val usizyInStock: Boolean,
    @SerializedName("usizy_quicksize_text")
    val usizyQuicksizeText: String,
    @SerializedName("usizy_size")
    var usizySize: String,
    @SerializedName("usizy_size_country")
    var usizySizeCountry: String,
    @SerializedName("usizy_size_country_system")
    val usizySizeCountrySystem: String,
    @SerializedName("usizy_size_result")
    val usizySizeResult: String
)