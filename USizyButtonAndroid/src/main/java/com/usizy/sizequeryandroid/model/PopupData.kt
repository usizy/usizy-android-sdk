package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

data class PopupData (
    @SerializedName("branding")
    val branding: String,
    @SerializedName("ck")
    val ck: String,
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("mass_system")
    val massSystem: String,
    @SerializedName("measurements")
    val measurements: List<String>,
    @SerializedName("metric_system")
    val metricSystem: String,
    @SerializedName("only_height")
    val onlyHeight: Boolean,
    @SerializedName("sex")
    val sex: String,
    @SerializedName("sex_limits")
    val sexLimits: SexLimits,
    @SerializedName("uid")
    val uid: String,
    @SerializedName("zone")
    val zone: String,
    @SerializedName("sk")
    val sk: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("logo")
    val logo: String,
    @SerializedName("shoe_system")
    val shoeSystem: String,
    @SerializedName("shoe_favs")
    val shoeFavs: List<ShoeFavs>
)