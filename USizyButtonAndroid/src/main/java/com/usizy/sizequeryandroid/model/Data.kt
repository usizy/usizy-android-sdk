package com.usizy.sizequeryandroid.model


import com.google.gson.annotations.SerializedName

data class Data(
        @SerializedName("imperial")
    val dataImperial: DataImperial?,
        @SerializedName("metric")
    val dataMetric: DataMetric?
)