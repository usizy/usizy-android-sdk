package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

class ShoeBrandRequest(
    @SerializedName("sex")
    var sex: String? = null,
    @SerializedName("q")
    var q: String? = null,
)