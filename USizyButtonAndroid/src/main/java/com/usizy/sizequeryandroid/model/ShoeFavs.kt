package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class ShoeFavs(
    @SerializedName("id")
    val id: Int,
    @SerializedName("text")
    val text: String,
    @SerializedName("thumb")
    val thumb: String,
    @SerializedName("sizes")
    val sizes: Any
)