package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

data class DisplayButtonResponse(
    @SerializedName("branding")
    val branding: String? = null,
    @SerializedName("ecommerce")
    val ecommerce: String? = null,
    @SerializedName("product")
    val product: String? = null,
    @SerializedName("show")
    val show: String? = null,
    @SerializedName("sk")
    val sk: String? = null,
    @SerializedName("usizy_in_stock")
    val usizyInStock: Boolean? = null,
    @SerializedName("usizy_quicksize_text")
    val usizyQuicksizeText: String? = null,
    @SerializedName("usizy_size")
    val usizySize: String? = null,
    @SerializedName("usizy_size_country")
    val usizySizeCountry: String? = null,
    @SerializedName("usizy_size_country_system")
    val usizySizeCountrySystem: String? = null,
    @SerializedName("usizy_size_result")
    val usizySizeResult: String? = null
)