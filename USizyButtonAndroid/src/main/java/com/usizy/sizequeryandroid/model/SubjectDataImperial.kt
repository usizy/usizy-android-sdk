package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

data class SubjectDataImperial(
        @SerializedName("age")
        var age: Int,
        @SerializedName("arm")
        var arm: Int,
        @SerializedName("arm_type")
        var armType: Int,
        @SerializedName("arm_width")
        var armWidth: Int,
        @SerializedName("back")
        var back: Int,
        @SerializedName("bike_fit")
        var bikeFit: Int,
        @SerializedName("chest")
        var chest: Float,
        @SerializedName("chest_type")
        var chestType: Int,
        @SerializedName("crotch")
        var crotch: Int,
        @SerializedName("fit")
        var fit: Int,
        @SerializedName("flexibility")
        var flexibility: Int,
        @SerializedName("foot")
        var foot: Float,
        @SerializedName("foot_type")
        var footType: Int,
        @SerializedName("foot_width")
        var footWidth: Int,
        @SerializedName("half_armspan")
        var halfArmspan: Int,
        @SerializedName("hand")
        var hand: Int,
        @SerializedName("hand_perimeter")
        var handPerimeter: Int,
        @SerializedName("height")
        var height: HeightImperial,
        @SerializedName("hips")
        var hips: Float,
        @SerializedName("hips_type")
        var hipsType: Int,
        @SerializedName("inseam")
        var inseam: Int,
        @SerializedName("leg_type")
        var legType: Int,
        @SerializedName("length")
        var length: Int,
        @SerializedName("long_inseam")
        var longInseam: Int,
        @SerializedName("lower_waist")
        var lowerWaist: Int,
        @SerializedName("morphology")
        var morphology: Int,
        @SerializedName("neck")
        var neck: Int,
        @SerializedName("outside_leg")
        var outsideLeg: Int,
        @SerializedName("reach")
        var reach: Int,
        @SerializedName("shoulder")
        var shoulder: Int,
        @SerializedName("stack")
        var stack: Int,
        @SerializedName("thigh")
        var thigh: Int,
        @SerializedName("waist")
        var waist: Float,
        @SerializedName("waist_type")
        var waistType: Int,
        @SerializedName("weight")
        var weight: Float,
        @SerializedName("wrist")
        var wrist: Int
)
