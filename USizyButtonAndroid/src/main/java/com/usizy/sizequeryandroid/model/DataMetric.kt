package com.usizy.sizequeryandroid.model


import com.google.gson.annotations.SerializedName

data class DataMetric(
        @SerializedName("baby")
    val baby: SubjectData?,

        @SerializedName("boy")
    val boy: SubjectData?,

        @SerializedName("child")
    val child: SubjectData?,

        @SerializedName("male")
    val male: SubjectData?,

        @SerializedName("girl")
    val girl: SubjectData?,

        @SerializedName("female")
    val female: SubjectData?
)