package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

class DisplayButtonRequest(
    @SerializedName("background")
    var background: String? = null,
    @SerializedName("branding")
    var branding: String? = null,
    @SerializedName("color")
    var color: String? = null,
    @SerializedName("guidelink")
    var guidelink: String? = null,
    @SerializedName("img")
    var img: String? = null,
    @SerializedName("style")
    var style: String? = null,
    @SerializedName("text")
    var text: String? = null
)