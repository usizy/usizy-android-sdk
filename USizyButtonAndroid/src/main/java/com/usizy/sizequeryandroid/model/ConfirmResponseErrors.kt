package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

class ConfirmResponseErrors(
    @SerializedName("non_field_errors")
    var non_field_errors: List<String>? = null,
    @SerializedName("ecommerce")
    var ecommerce: List<String>? = null,
    @SerializedName("order_id")
    var order_id: List<String>? = null,
    @SerializedName("product_ids")
    var product_ids: List<String>? = null,
    @SerializedName("variation_ids")
    var variation_ids: List<String>? = null,
    @SerializedName("sizes")
    var sizes: List<String>? = null,
    @SerializedName("sizes_system")
    var sizes_system: List<String>? = null,
    @SerializedName("prices_no_vat")
    var prices_no_vat: List<String>? = null,
    @SerializedName("prices_vat")
    var prices_vat: List<String>? = null,
    @SerializedName("total_no_vat")
    var total_no_vat: List<String>? = null,
    @SerializedName("total_vat")
    var total_vat: List<String>? = null,
    @SerializedName("shipping_cost")
    var shipping_cost: List<String>? = null,
    @SerializedName("currency")
    var currency: List<String>? = null,
)
