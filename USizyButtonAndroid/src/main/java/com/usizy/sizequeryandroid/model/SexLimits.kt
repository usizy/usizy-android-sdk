package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

data class SexLimits(
        @SerializedName("imperial")
    val imperial: LimitsImperial,
        @SerializedName("metric")
    val metric: LimitsMetric
)