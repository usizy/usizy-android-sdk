package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

class SaveRecommendRequest(
    @SerializedName("age")
    var age: Int? = null,
    @SerializedName("arm")
    var arm: String? = null,
    @SerializedName("arm_type")
    var armType: Int? = null,
    @SerializedName("arm_width")
    var armWidth: String? = null,
    @SerializedName("back")
    var back: String? = null,
    @SerializedName("bike_fit")
    var bikeFit: Int? = null,
    @SerializedName("chest")
    var chest: String? = null,
    @SerializedName("chest_type")
    var chestType: Int? = null,
    @SerializedName("ck")
    var ck: String? = null,
    @SerializedName("crotch")
    var crotch: String? = null,
    @SerializedName("fit")
    var fit: Int? = null,
    @SerializedName("flexibility")
    var flexibility: Int? = null,
    @SerializedName("foot")
    var foot: Float? = null,
    @SerializedName("foot_type")
    var footType: Int? = null,
    @SerializedName("foot_width")
    var footWidth: Int? = null,
    @SerializedName("half_armspan")
    var halfArmspan: String? = null,
    @SerializedName("hand")
    var hand: String? = null,
    @SerializedName("hand_perimeter")
    var handPerimeter: String? = null,
    @SerializedName("head")
    var head: String? = null,
    @SerializedName("height")
    var height: String? = null,
    @SerializedName("hips")
    var hips: String? = null,
    @SerializedName("hips_type")
    var hipsType: Int? = null,
    @SerializedName("inseam")
    var inseam: String? = null,
    @SerializedName("leg_type")
    var legType: Int? = null,
    @SerializedName("length")
    var length: String? = null,
    @SerializedName("long_inseam")
    var longInseam: String? = null,
    @SerializedName("lower_waist")
    var lowerWaist: String? = null,
    @SerializedName("mass_system")
    var massSystem: String? = null,
    @SerializedName("metric_system")
    var metricSystem: String? = null,
    @SerializedName("neck")
    var neck: String? = null,
    @SerializedName("outside_leg")
    var outsideLeg: String? = null,
    @SerializedName("reach")
    var reach: String? = null,
    @SerializedName("sex")
    var sex: String? = null,
    @SerializedName("shoe_brand")
    var shoeBrand: Int? = null,
    @SerializedName("shoe_size")
    var shoeSize: String? = null,
    @SerializedName("shoulder")
    var shoulder: String? = null,
    @SerializedName("sizesystem")
    var sizesystem: String? = null,
    @SerializedName("stack")
    var stack: String? = null,
    @SerializedName("stockSizes")
    var stockSizes: List<String>? = null,
    @SerializedName("thigh")
    var thigh: String? = null,
    @SerializedName("w")
    var w: String? = null,
    @SerializedName("waist")
    var waist: String? = null,
    @SerializedName("waist_type")
    var waistType: Int? = null,
    @SerializedName("weight")
    var weight: String? = null,
    @SerializedName("wrist")
    var wrist: String? = null,
    @SerializedName("sk")
    var sk: String? = null,
    @SerializedName("user")
    var user: String? = null
)