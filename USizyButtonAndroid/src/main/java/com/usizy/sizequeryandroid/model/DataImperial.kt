package com.usizy.sizequeryandroid.model


import com.google.gson.annotations.SerializedName

data class DataImperial(
        @SerializedName("baby")
    val baby: SubjectDataImperial?,

        @SerializedName("boy")
    val boy: SubjectDataImperial?,

        @SerializedName("child")
    val child: SubjectDataImperial?,

        @SerializedName("male")
     val male: SubjectDataImperial?,

        @SerializedName("female")
    val female: SubjectDataImperial?,

        @SerializedName("girl")
    val girl: SubjectDataImperial?
)