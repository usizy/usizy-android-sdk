package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

data class HeightLimitImperial(
        @SerializedName("max")
        val max: HeightImperial,
        @SerializedName("min")
        val min: HeightImperial,
        @SerializedName("step")
        val step: Float
)
