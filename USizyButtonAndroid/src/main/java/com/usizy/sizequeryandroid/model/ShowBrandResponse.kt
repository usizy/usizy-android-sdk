package com.usizy.sizequeryandroid.model

import com.google.gson.annotations.SerializedName

data class ShowBrandResponse (
    @SerializedName("status")
    val status: String,
    @SerializedName("results")
    val shoeFavs: List<ShoeFavs>
)