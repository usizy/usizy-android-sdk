package com.usizy.sizequeryandroid

class SizeQueryException(message: String): Exception(message)