package com.usizy.sizequeryandroid

object ShowProduct {
    const val APP = "show_product_app"
    const val ERROR = "show_product_error"
}

object PopupZones {
    const val SHOES = "shoes"
    const val LOWER = "lower"
    const val UPPER = "upper"
    const val FULL = "full"
}

object SizeResult {
    const val MATCH = "MATCH"
    const val OVERFLOW = "OVERFLOW"
    const val UNDERFLOW = "UNDERFLOW"
}

object MeasurementsValues {
    const val HEIGHT = "height"
    const val CHEST = "chest"
    const val WAIST = "waist"
    const val HIPS = "hips"
}

object SexValues {
    const val BABY = "baby"
    const val BOY = "boy"
    const val CHILD = "child"
    const val GIRL = "girl"
    const val FEMALE = "female"
    const val MALE = "male"
    const val HUMAN = "human"
    const val UNISEX = "unisex"
}

object ZoneValues {
    const val UPPER = "upper"
    const val LOWER = "lower"
    const val FULL = "full"
}

object MetricSystem {
    const val IMPERIAL = "imperial"
    const val METRIC = "metric"
}

object RecommendationWay {
    const val BASIC_WAY = "basic"
    const val ADV_WAY = "adv"
}