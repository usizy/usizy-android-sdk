package com.usizy.sizequeryandroid

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.usizy.sizequeryandroid.model.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.usizy.sizebutton.cache.SizeCache
import com.usizy.sizebutton.utils.LocaleUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext
import java.io.UnsupportedEncodingException
import java.lang.Exception
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.math.floor


class SizeQuery constructor(
    private val context: Context,
    private val user: String = "AnonymousUser",
    private val baseDir: String = "usizy.com/api/v1",
    private val scheme: String = "https",
    private val confirmUrl: String = "https://usizy.com/api/v2/event/confirm"
) : ViewModel() {

    private val scope = CoroutineScope(
        Job() + Dispatchers.IO
    )
    private val queue: RequestQueue = Volley.newRequestQueue(context)

    suspend fun openPopup(product: String): PopupData {
        val sk = SizeCache.getInstance(context).cacheSK

        val params: MutableMap<String, Any?> = HashMap()
        params["user"] = user
        params["product"] = product
        params["ref"] = context.applicationContext.packageName
        params["language"] = LocaleUtils.getCurrentLanguage()
        params["sk"] = sk

        val service = "open-popup"
        val url = "$scheme://$baseDir/$service"

        return withContext(scope.coroutineContext) {
            val data: Pair<String, Boolean> = baseGetObject(Request.Method.POST, url, params)
            if (data.second) {
                throw SizeQueryException(data.first)
            }
            Log.d("openPopup", data.first)
            return@withContext try {
                Gson().fromJson<PopupData>(data.first, PopupData::class.java)
            } catch(e: Exception) {
                Log.e("SizeQuery",  "To json exception ${e.message}")
                throw SizeQueryException("${e.message}")
            }
        }
    }

    suspend fun displayButton(product: String): DisplayButtonResponse {
        val sk = SizeCache.getInstance(context).cacheSK

        val params: MutableMap<String, Any?> = HashMap()
        params["user"] = user
        params["product"] = product
        params["ref"] = context.applicationContext.packageName
        params["language"] = LocaleUtils.getCurrentLanguage()
        params["sk"] = sk

        val service = "display-button"
        val url = "$scheme://$baseDir/$service"

        return withContext(scope.coroutineContext) {
            val data: Pair<String, Boolean> = baseGetObject(Request.Method.POST, url, params)
            if (data.second) {
                throw SizeQueryException(data.first)
            }
            Log.d("displayButton", data.first)
            return@withContext try {
                Gson().fromJson<DisplayButtonResponse>(data.first, DisplayButtonResponse::class.java)
            } catch(e: Exception) {
                Log.e("SizeQuery",  "To json exception ${e.message}")
                throw SizeQueryException("${e.message}")
            }
        }
    }

    suspend fun saveRecommend(product: String, request: SaveRecommendRequest): SaveRecommendResponse {
        val sk = SizeCache.getInstance(context).cacheSK

        val params: MutableMap<String, Any?> = HashMap(request.serializeToMap())
        params["product"] = product
        params["ref"] = context.applicationContext.packageName
        params["language"] = LocaleUtils.getCurrentLanguage()
        params["sk"] = sk

        if (request.metricSystem.equals(MetricSystem.IMPERIAL) && request.height != null){
            val height = request.height?.toFloat()
            val ft = floor(height!! / 12)
            val inx = height % 12
            params["height"] = HeightImperial(ft, inx)
        }

        val service = "save-recommend"
        val url = "$scheme://$baseDir/$service"

        return withContext(scope.coroutineContext) {
            val data: Pair<String, Boolean> = baseGetObject(Request.Method.POST, url, params)
            if (data.second) {
                throw SizeQueryException(data.first)
            }
            Log.d("saveRecommend", data.first)
            Gson().fromJson<SaveRecommendResponse>(data.first, SaveRecommendResponse::class.java)
        }
    }

    suspend fun shoeBrand(product: String, request: ShoeBrandRequest): ShowBrandResponse {

        val params: MutableMap<String, Any?> = HashMap(request.serializeToMap())
        params["product"] = product
        params["ref"] = context.applicationContext.packageName
        params["limit"] = 12

        val service = "shoe-brand"
        val url = "$scheme://$baseDir/$service"

        return withContext(scope.coroutineContext) {
            val data: Pair<String, Boolean> = baseGetObject(Request.Method.POST, url, params)
            if (data.second) {
                throw SizeQueryException(data.first)
            }
            Log.d("shoeBrand", data.first)
            Gson().fromJson<ShowBrandResponse>(data.first, ShowBrandResponse::class.java)
        }
    }

    suspend fun confirm(request: ConfirmRequest): ConfirmResponseErrors? {
        val sk = SizeCache.getInstance(context).cacheSK

        val params: MutableMap<String, Any?> = HashMap(request.serializeToMap())
        params["ref"] = context.applicationContext.packageName
        params["language"] = LocaleUtils.getCurrentLanguage()
        params["sk"] = sk

        return withContext(scope.coroutineContext) {
            val data: Pair<String, Boolean> = baseGetObject(Request.Method.POST, confirmUrl, params)
            if (data.second) {
                throw SizeQueryException(data.first)
            }
            Log.d("confirm", data.first)
            Gson().fromJson<ConfirmResponseErrors>(data.first, ConfirmResponseErrors::class.java)
        }
    }

    private suspend fun baseGetObject(
        method: Int,
        url: String,
        parameters: MutableMap<String, Any?>
    ) = suspendCoroutine { cont: Continuation<Pair<String, Boolean>> ->
        val stringRequest = getStringRequest(method, url, parameters, cont)
        queue.add(stringRequest)
    }

    private fun getStringRequest(
        method: Int,
        url: String,
        parameters: MutableMap<String, Any?>,
        cont: Continuation<Pair<String, Boolean>>
    ): StringRequest {
        return  object: StringRequest(
            method,
            url,
            Response.Listener<String> { response ->
                cont.resume(Pair(response, false))
            },
            Response.ErrorListener {
                val err = it.message
                val data = if(it.networkResponse != null && it.networkResponse.data != null) it.networkResponse.data.toString() else ""

                Log.e("SizeQuery", "$err $data")
                cont.resume(Pair("SizeQuery Error $err", true))
            }){
            @Override
            override fun getBody(): ByteArray? {
                return try {
                    Gson().toJson(parameters).encodeToByteArray()
                } catch (e: Exception) {
                    Log.e("SizeQuery",  "To json exception ${e.message}")
                    throw SizeQueryException("To json exception ${e.message}")
                }
            }
            @Override
            override fun getHeaders(): Map<String, String?> {
                val headers = HashMap<String, String>()
                headers["content-type"] = "application/json"
                headers["Accept"] = "application/json"
                return headers
            }
            @Override
            override fun parseNetworkResponse(response: NetworkResponse): Response<String> {
                var parsed: String

                val encoding = charset(HttpHeaderParser.parseCharset(response.headers))

                try {
                    parsed = String(response.data, encoding)
                    val bytes = parsed.toByteArray(encoding)
                    parsed = String(bytes, charset("UTF-8"))

                    return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response))
                } catch (e: UnsupportedEncodingException) {
                    return Response.error(ParseError(e))
                }
            }
        }
    }

    //convert a data class to a map
    fun <T> T.serializeToMap(): Map<String, Any> {
        return convert()
    }

    //convert an object of type I to type O
    inline fun <I, reified O> I.convert(): O {
        val json = Gson().toJson(this)
        return Gson().fromJson(json, object : TypeToken<O>() {}.type)
    }
}